local root = getRootElement()

local ElementTypes =  { "object", "spawnpoint", "checkpoint", "pickup", "marker", "racepickup", "radararea", "colshape", "blip" }

function batchMoveElements(elements, dx, dy, dz)
	for _,e in ipairs(elements) do
		local x, y, z = getElementPosition(e)
		x = x + dx
		y = y + dy
		z = z + dz
		setElementPosition(e, x, y, z)
		triggerEvent("syncProperty", root, "position", {x, y, z}, e)
	end
end

function consoleMoveObjects(playerSource, command, dx, dy, dz, elementTypes)
	
	for _,elementType in pairs(ElementTypes) do
		if (elementTypes and elementType == elementTypes) or not elementTypes then
			local elements = getElementsByType(elementType)
			if elements then
				batchMoveElements(elements, dx, dy, dz)
			end
		end
	end
end

addCommandHandler("mmove", consoleMoveObjects)