    local models = {10890, 10889, 1350, 1226, 1308}
     
    addEventHandler("onClientResourceStart",resourceRoot,
            function()
                    setCameraMatrix(3000,3000,3000)
                    for _,model in pairs(models) do
                            removeWorldModel(model, 10890, 1610.38, -1655.07, 32.2969)
                    end
                    setTimer(setCameraTarget,1000,1,getLocalPlayer())
            end
    )
     
    addEventHandler("onClientResourceStop",resourceRoot,
            function()
                    setCameraMatrix(3000,3000,3000)
                    for _,model in pairs(models) do
                            restoreWorldModel(model, 3757, 1610.38, -1655.07, 32.2969)
                    end
                    setTimer(setCameraTarget,1000,1,getLocalPlayer())
            end
    )
