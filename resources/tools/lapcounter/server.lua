
CheckpointIDs = {}
TotalLaps = 0

addEventHandler("onMapStarting", root, 
	function()
		CheckpointIDs = {}
		TotalLaps = 0
		
		-- fill in checkpoint id table
		local checkpoints = getElementsByType("checkpoint")
		for _, checkpoint in ipairs(checkpoints) do
			local id = getElementID(checkpoint)
			table.insert(CheckpointIDs, id)
			if string.find(id, "lap ", 1, true) then
				local lap = string.sub(id, 5)
				if tonumber(lap) then
					TotalLaps = math.max(TotalLaps, lap)
				end
			end
		end
		
		if TotalLaps ~= 0 then
			triggerClientEvent("onClientSetLap", resourceRoot, 0, TotalLaps)
		end
	end
)


addEventHandler("onPlayerReachCheckpoint", root, 
	function(i, t)
		local id = CheckpointIDs[i]
		if string.find(id, "lap ", 1, true) then
			local lap = string.sub(id, 5)
			if tonumber(lap) then
				triggerClientEvent(source, "onClientSetLap", resourceRoot, tonumber(lap), TotalLaps)
			end
		elseif id == "endlaps" then
			triggerClientEvent(source, "onClientReachFinalSection", source)
		end
	end
)