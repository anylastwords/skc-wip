
addEvent("onClientSetLap", true)
addEventHandler("onClientSetLap", root,
	function(lap, totalLaps)
		if lap ~= 0 then
			if lap == totalLaps then
				DisplayPager("Starting final lap!", 7500, nil, 5, "white-flag.png")
			else
				DisplayPager(string.format("Starting lap %u of %u", lap, totalLaps), 7500, nil, 5)
			end
		end
	end
)


addEvent("onClientReachFinalSection", true)
addEventHandler("onClientReachFinalSection", root,
	function()
		DisplayPager("Final section...", 7500, nil, 5)
	end
)
