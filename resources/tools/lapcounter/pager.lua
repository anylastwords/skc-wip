local sx, sy = guiGetScreenSize()
local margin = 400
local marginTop = sy - 10 - 80
local padding = 10
local width = sx - margin * 2
local height = 80
local bgColor = tocolor(8, 8, 8, 160)
local textColor = tocolor(255, 255, 255, 255)
local text = nil
local font = "pricedown"
local image = nil

function DisplayPager(message, timeout, color, sound, img)
	text = message
	image = nil
	textColor = tocolor(255, 255, 255, 255)
	addEventHandler("onClientRender", root, Render)
	if not timeout then
		timeout = 10000
	end
	if color then
		textColor = color
	end
	if img then
		image = img
	end
	setTimer(HidePager, timeout, 1)
	if tonumber(sound) then
		playSoundFrontEnd(tonumber(sound))
	elseif sound and type(sound) == "boolean" then
		playSound("pager.wav")
	end
end

function HidePager()
	removeEventHandler("onClientRender", root, Render)
	text = nil
	textColor = tocolor(255, 255, 255, 255)
end

function Render()
	dxDrawRectangle(margin, marginTop, width, height, bgColor, true)
	local tx = margin + padding
	local ty = marginTop + padding
	dxDrawText (text, tx , ty, margin+width-padding, marginTop+height-padding, textColor, 1.5, font, "center", "center", true, true, true)
	if image then
		dxDrawImage(margin + 2 * padding, marginTop + padding, 64, 64, image, 0, 0, 0, tocolor(255, 255, 255, 255), true)
	end
end
