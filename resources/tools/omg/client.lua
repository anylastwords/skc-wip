-- DDC Object Movement Generator beta!
objs = {}
xmlo = {}
point = {}
attach = {}
dx = { scale = 0.05, dist = 500 }
selected = 0
omgAreTesting = false
ptW, ptH, fr = 64, 64, 0
sWidth, sHeight = guiGetScreenSize()
color = {dxAlpha = 128, dxAlphaTest = 24, imgAlpha = 255, imgAlphaTest = 50}
white = tocolor(255,255,255,color.dxAlpha)
imgcol = tocolor(255,255,255,color.imgAlpha)
black = tocolor(0,0,0,128)
imgs = "img/startpoint.png"
img = "img/arrows.png"
imgr = "img/rotation.png"
imgp = "img/point.png"
imgarrow = "img/arrow.png"
imgattach = "img/attach.png"
imgrattach = "img/rattach.png"
logo = "img/omg.png"
resRoot = getResourceRootElement(getThisResource())
currentObject = nil
offScreen = 1000
xml = false

addEventHandler("onClientResourceStart", resRoot, 
	function ()
    sWidth, sHeight = guiGetScreenSize()
    addEventHandler("onClientRender", getRootElement(), renderPoints)
    omgIcon = guiCreateStaticImage(sWidth - 76, sHeight - 76, 64, 64, logo, false)

		omgLuaWindow = guiCreateWindow(sWidth/2 - 300, sHeight/2 - 200, 600, 333, "Lua script:", false)
    omgLuaScript = guiCreateMemo(.02, .05, .98, .83, "script\nhere", true, omgLuaWindow)
    omgLuaClose = guiCreateButton(.9, .9, .1, .1, "Close", true, omgLuaWindow)
    guiSetFont(guiCreateLabel(.03, .89, .5, .05, "                 Your map resource name:",true, omgLuaWindow), "default")
    guiSetFont(guiCreateLabel(.03, .94, .5, .05, "Lua file will be saved there and added to meta.xml",true, omgLuaWindow), "default-small")
    omgLuaResName = guiCreateEdit(.39, .9, .3, .1, "", true, omgLuaWindow)
    omgLuaSave = guiCreateButton(.69, .9, .2, .1, "Save Lua file", true, omgLuaWindow)

		omgErrorWindow = guiCreateWindow(sWidth/2 - 200, sHeight/2 - 50, 400, 100, "OMG Error!", false)
    omgErrorText = guiCreateLabel(.1, .35, .94, .3, "Error:",true, omgErrorWindow)
    omgErrorClose = guiCreateButton(.3, .7, .4, .3, "OK, I got it", true, omgErrorWindow)
    
		omgXMLWindow = guiCreateWindow(sWidth - 272, 30, 250, 330, "OMG: Object Library", false)
    objXMLList = guiCreateGridList(.04, .08, .95, .8, true, omgXMLWindow)
    guiSetFont(objXMLList, "default-small")
    objXMLListName = guiGridListAddColumn(objXMLList, "object name", 0.6)
    objXMLListModel = guiGridListAddColumn(objXMLList, "model", 0.3)
    omgXMLLoadObject = guiCreateButton(.04, .9, .4, .1, "Load Selected", true, omgXMLWindow)
    omgXMLDeleteObject = guiCreateButton(.45, .9, .39, .1, "Delete from Library", true, omgXMLWindow)
    guiSetFont(omgXMLLoadObject, "default-bold-small")
    guiSetFont(omgXMLDeleteObject, "default-small")
    omgXMLClose = guiCreateButton(.85, .9, .11, .1, "X", true, omgXMLWindow)
    
		omgWindow = guiCreateWindow(sWidth - 272, sHeight - 350, 250, 330, "DDC ObjectMovementGenerator", false)
    omgDisplay = guiCreateCheckBox(.04, .08, .6, .05, "Display all objects/points", false, true, omgWindow)
    omgListXML = guiCreateButton(.65, .08, .3, .05, "Load object...", true, omgWindow)
    guiSetFont(omgListXML, "default-small")
    guiSetFont(omgDisplay, "default-small")
    objList = guiCreateGridList(.04, .14, .95, .55, true, omgWindow)
    guiSetFont(objList, "default-small")
    objListName = guiGridListAddColumn(objList, "object:points", 0.6)
    objListModel = guiGridListAddColumn(objList, "model", 0.3)
   
    omgUpdateList()
    omgAdd = guiCreateButton(.04, .70, .3, .07, "Add Object", true, omgWindow)
    omgEdit = guiCreateButton(.35, .70, .3, .07, "Edit Object", true, omgWindow)
    omgListTest = guiCreateButton(.66, .7, .3, .07, "Test Object", true, omgWindow)
    
    omgDelete = guiCreateButton(.04, .78, .3, .07, "Delete", true, omgWindow)
    omgDeleteAll = guiCreateButton(.35, .78, .3, .07, "Delete All", true, omgWindow)
    omgSaveXML = guiCreateButton(.66, .78, .3, .07, "Save selected", true, omgWindow)
    
    guiSetFont(guiCreateLabel(.06, .85, .5, .05, "Script name:",true, omgWindow), "default-small")
    omgExportName = guiCreateEdit(.01, .9, .47, .07, "", true, omgWindow)
    omgExport = guiCreateButton(.51, .9, .26, .1, ".lua", true, omgWindow)
    omgClose = guiCreateButton(.85, .9, .11, .1, "X", true, omgWindow)
    guiSetFont(omgExport, "default-bold-small")
    guiSetFont(omgEdit, "default-bold-small")
    guiSetFont(omgAdd, "default-bold-small")
    guiSetFont(omgListTest, "default-bold-small")
    guiSetFont(omgDelete, "default-small")
    guiSetFont(omgDeleteAll, "default-small")
    guiSetFont(omgSaveXML, "default-small")

		omgObjWindow = guiCreateWindow(sWidth - 333, sHeight - 500, 300, 400, "OMG Object!", false)
    omgObjAction = guiCreateLabel(.04, .05, .9, .05, "Object to be added to the script:",true, omgObjWindow)
    guiSetFont(omgObjAction, "default-bold-small")
    omgObjName = guiCreateEdit( .3, .1, .65, .05, "", true, omgObjWindow)     
    guiCreateLabel(.04, .11, .3, .07, "Object name:",true, omgObjWindow)
    omgObjModel = guiCreateEdit( .3, .15, .5, .05, "", true, omgObjWindow)     
    omgObjGetModel = guiCreateButton(.8, .15, .15, .05, "GET", true, omgObjWindow)
    guiCreateLabel(.04, .16, .3, .05, "Model ID:",true, omgObjWindow)

    guiSetFont(guiCreateLabel(.04, .23, .9, .05, "Add point:",true, omgObjWindow), "default-bold-small")
    omgObjSget = guiCreateButton(.3, .23, .65, .05, "Get location of selected object", true, omgObjWindow)
    omgObjSx = guiCreateEdit( .3, .28, .22, .05, "", true, omgObjWindow)     
    omgObjSy = guiCreateEdit( .52, .28, .22, .05, "", true, omgObjWindow)     
    omgObjSz = guiCreateEdit( .74, .28, .21, .05, "", true, omgObjWindow)     
    guiCreateLabel(.04, .28, .3, .05, "Position:",true, omgObjWindow)
    guiCreateLabel(.04, .33, .3, .05, "Rotation:",true, omgObjWindow)
    omgObjSrx = guiCreateEdit( .3, .33, .22, .05, "", true, omgObjWindow)     
    omgObjSry = guiCreateEdit( .52, .33, .22, .05, "", true, omgObjWindow)     
    omgObjSrz = guiCreateEdit( .74, .33, .21, .05, "", true, omgObjWindow)     
    guiCreateLabel(.04, .38, .3, .05, "Move time:",true, omgObjWindow)
    omgObjMtime = guiCreateEdit( .3, .38, .22, .05, "5000", true, omgObjWindow)     
    guiCreateLabel(.54, .38, .3, .05, "Stop for:",true, omgObjWindow)
    omgObjStime = guiCreateEdit( .74, .38, .21, .05, "0", true, omgObjWindow)     
    omgObjPadd = guiCreateButton(.04, .43, .48, .05, "Add (scroll: Edit)", true, omgObjWindow)
    omgObjPaddFrom = guiCreateButton(.52, .43, .43, .05, "Add new from object", true, omgObjWindow)
    omgObjPdelete = guiCreateButton(.04, .48, .48, .05, "Delete selected point", true, omgObjWindow)
    omgObjPclear = guiCreateButton(.04, .53, .48, .05, "Delete all points", true, omgObjWindow)
    omgObjRelocate = guiCreateButton(.52, .48, .43, .05, "Relocate all by startpoint", true, omgObjWindow)
    guiSetFont(omgObjSget, "default-small")
    guiSetFont(omgObjPadd, "default-bold-small")
    guiSetFont(omgObjPaddFrom, "default-bold-small")
    guiSetFont(omgObjPdelete, "default-small")
    guiSetFont(omgObjPclear, "default-small")
    guiSetFont(omgObjRelocate, "default-small")
   
    omgObjRonly = guiCreateCheckBox(.04, .6, .43, .05, "Only rotate in place:", false, true, omgObjWindow)
    omgObjNoReturn = guiCreateCheckBox(.52, .6, .48, .05, "Stop at last point", false, true, omgObjWindow)
    guiSetFont(omgObjRonly, "default-bold-small")
    guiSetFont(omgObjNoReturn, "default-bold-small")
    guiCreateLabel(.04, .65, .3, .05, "Rotate X-Y-Z :",true, omgObjWindow)
    omgObjSmrx = guiCreateEdit( .3, .65, .22, .05, "0", true, omgObjWindow)     
    omgObjSmry = guiCreateEdit( .52, .65, .22, .05, "0", true, omgObjWindow)     
    omgObjSmrz = guiCreateEdit( .74, .65, .22, .05, "0", true, omgObjWindow)     
    guiCreateLabel(.04, .7, .3, .05, "Rotation time:",true, omgObjWindow)
    omgObjRMtime = guiCreateEdit( .3, .7, .22, .05, "5000", true, omgObjWindow)     
    guiCreateLabel(.54, .7, .3, .05, "Pause:",true, omgObjWindow)
    omgObjRStime = guiCreateEdit( .74, .7, .22, .05, "0", true, omgObjWindow)     

    omgObjAttCount = guiCreateLabel(.04, .77, .9, .05, "Objects attached: 0",true, omgObjWindow)
    omgObjAttAdd = guiCreateButton(.04, .82, .48, .05, "Attach selected object", true, omgObjWindow)
    omgObjAttClear = guiCreateButton(.52, .82, .43, .05, "Delete all attachments", true, omgObjWindow)
    guiSetFont(omgObjAttCount, "default-bold-small")
    guiSetFont(omgObjAttAdd, "default-bold-small")
    guiSetFont(omgObjAttClear, "default-small")
    
    omgObjTest = guiCreateButton(.04, .92, .48, .05, "Test movement", true, omgObjWindow)
    omgObjSave = guiCreateButton(.52, .92, .35, .05, "Save Object", true, omgObjWindow)
    omgObjClose = guiCreateButton(.87, .92, .08, .05, "X", true, omgObjWindow)
    guiSetFont(omgObjTest, "default-bold-small")
    guiSetFont(omgObjSave, "default-bold-small")

    guiSetFont(omgObjWindow, "default-small")
    guiSetVisible(omgWindow, false)
		guiSetVisible(omgLuaWindow, false)
		guiWindowSetSizable(omgLuaWindow, false)		
		guiWindowSetSizable(omgWindow, false)		
		guiSetVisible(omgErrorWindow, false)
		guiWindowSetSizable(omgErrorWindow, false)		
		guiSetVisible(omgObjWindow, false)
		guiWindowSetSizable(omgObjWindow, false)		
    guiSetVisible(omgXMLWindow, false)
		guiWindowSetSizable(omgXMLWindow, false)		

    addEventHandler("onClientMouseEnter", source,
			function()
    		if omgDisableOthers(source) then guiSetInputEnabled(true) end  
      end      
    )  
    addEventHandler("onClientMouseLeave", source,
			function()
    		if omgDisableOthers(source) then guiSetInputEnabled(false) end  
      end      
    )  
    addEventHandler("onClientGUIClick", source,
			function()
				if source == omgIcon then 
      		guiSetVisible(omgWindow, true)
          guiBringToFront(omgWindow)
				elseif source == omgClose then guiSetVisible(omgWindow, false)
				elseif source == omgXMLClose then guiSetVisible(omgXMLWindow, false)
				elseif source == omgListXML then 
          guiSetVisible(omgXMLWindow, true)
          omgUpdateXMLList()
				elseif source == omgLuaClose then 
      		guiSetVisible(omgLuaWindow, false)
          guiSetInputEnabled(false)
				elseif source == omgAdd then 
          currentObject = nil
          omgObjClear()
      		guiSetVisible(omgObjWindow, true)
          guiBringToFront(omgObjWindow)
				elseif source == omgEdit then omgDoEdit()
				elseif source == omgDelete then omgDoDelete()
				elseif source == omgDeleteAll then omgDoDeleteAll()
				elseif source == omgExport then 
          guiSetText(omgExportName, omgFormat(guiGetText(omgExportName), true))
      		guiSetVisible(omgLuaWindow, true)
          guiBringToFront(omgLuaWindow)
          guiSetInputEnabled(true)
          guiSetText(omgLuaScript, omgDoExport())
				elseif source == omgLuaSave then omgFileSave()
				elseif source == omgObjClose then
          omgHalt()
          omgResetPoints()
      		guiSetVisible(omgObjWindow, false)
          guiSetInputEnabled(false)
				elseif source == omgErrorClose then guiSetVisible(omgErrorWindow, false)
				elseif source == omgObjGetModel then omgSetModel()
				elseif source == omgObjSget then omgSetStart()
  			elseif source == omgObjPadd then 
          if selected ~= 0 then
            guiSetText(omgObjPadd, "Add (scroll: Edit)")
            guiSetText(omgObjPaddFrom, "Add new from object")
            omgObjAddPoint(selected)
            selected = 0
          else  
            omgObjAddPoint()
          end  
				elseif source == omgObjPaddFrom then 
          if selected ~= 0 then
            guiSetText(omgObjPadd, "Add (scroll: Edit)")
            guiSetText(omgObjPaddFrom, "Add new from object")
            omgSetStart()
            omgObjAddPoint(selected)
            selected = 0
          else
            omgSetStart()
      		  omgObjAddPoint()
          end  
				elseif source == omgObjPclear then omgResetPoints()
				elseif source == omgObjPdelete then	omgDeleteLastPoint(selected)
				elseif source == omgObjAttAdd then omgObjAddAttach()
				elseif source == omgObjAttClear then omgResetAttaches()
				elseif source == omgObjTest then 
          if omgAreTesting then omgHalt()
          else omgTestObject() end
				elseif source == omgListTest then 
          if omgAreTesting then omgHalt()
          else 
            local name = gettok(guiGridListGetItemText(objList, guiGridListGetSelectedItem(objList), 1), 1, 58)
            if objs[name] and #objs[name].points > 0 then omgTestObject(name) end        
          end
				elseif source == omgObjSave then 
      		if omgSaveObject() then
            omgHalt()
            omgObjClear()
            omgResetPoints()
            omgResetAttaches()
            omgUpdateList()
            guiSetVisible(omgObjWindow, false)
          end  
				elseif source == omgObjRelocate then omgRelocateObject()
				elseif source == objList then omgViewObject()
				elseif source == omgSaveXML then 
          local name = gettok(guiGridListGetItemText(objList, guiGridListGetSelectedItem(objList), 1), 1, 58)
          if objs[name] and #objs[name].points > 0 then 
            xmlSaveObject(name) 
            guiSetVisible(omgXMLWindow, true)
            omgUpdateXMLList()
          end        
				elseif source == omgXMLDeleteObject then 
          local name = guiGridListGetItemText(objXMLList, guiGridListGetSelectedItem(objXMLList), 1)
          if name and name ~= ""  then 
            xmlDeleteObject(name) 
            omgUpdateXMLList()
          end        
				elseif source == omgXMLLoadObject then 
          local name = guiGridListGetItemText(objXMLList, guiGridListGetSelectedItem(objXMLList), 1)
          if name and name ~= ""  then 
            xmlLoadObject(name) 
          end        
        end
			end
		)
    addEventHandler("onClientGUIDoubleClick", source,
      function()
				if source == objList then omgMoveCamera()
        end
      end
    )
    addEventHandler("onClientMouseWheel", source,
      function(updown)
        if source == omgObjPadd and #point > 0 then  
          selected = selected + updown
          if selected < 1 then selected = #point
          elseif selected > #point then selected = 1 end
          guiSetText(omgObjPadd, "Edit point: "..selected)
          guiSetText(omgObjPaddFrom, "Selected to point "..selected)
          guiSetText(omgObjSx, point[selected].x)
          guiSetText(omgObjSy, point[selected].y)
          guiSetText(omgObjSz, point[selected].z)
          guiSetText(omgObjSrx, point[selected].rx)
          guiSetText(omgObjSry, point[selected].ry)
          guiSetText(omgObjSrz, point[selected].rz)
          guiSetText(omgObjMtime, point[selected].mt)
          guiSetText(omgObjStime, point[selected].st)
        end
      end
    )
    xml = xmlLoadFile("objects.xml")
    if not xml then xml = xmlCreateFile("objects.xml", "objects") end
	end
)

function omgSetModel()
  local id = getElementModel(exports["editor_main"]:getSelectedElement())
  if id then 
    guiSetText(omgObjModel, id) 
  else
    omgError("No object selected!")  
  end
end

function omgSetStart()
  local obj = exports["editor_main"]:getSelectedElement()
  if obj then
    local x,y,z = getElementPosition(obj)  
    local rx,ry,rz = getElementRotation(obj)  
    if x and y and z then
      guiSetText(omgObjSx, x)
      guiSetText(omgObjSy, y)
      guiSetText(omgObjSz, z)
    end  
    if rx and ry and rz then
      guiSetText(omgObjSrx, rx)
      guiSetText(omgObjSry, ry)
      guiSetText(omgObjSrz, rz)
    end
  else
    omgError("No object selected!")  
  end
end

function omgTestObject(listname)
  if listname then
    omgAreTesting = true
    white = tocolor(255,255,255,color.dxAlphaTest)
    imgcol = tocolor(255,255,255,color.imgAlphaTest)
    local test = table.copy(objs[listname])
    guiSetText(omgListTest, "STOP")
    triggerServerEvent("omgDoObjectTest", getLocalPlayer(), test, getElementDimension(getLocalPlayer()), true)
  else
    local err = false
    guiSetText(omgObjName, omgFormat(guiGetText(omgObjName)))
    if (#point == 0) then err = "No movement points added!" end
    if (#point == 1) and (not guiCheckBoxGetSelected(omgObjRonly)) then err = "You have only 1 point, add more or set object rotate-only!" end
    if (guiGetText(omgObjModel) == "") then err = "No object model specified!" end
    if err then
      omgError(err)
      return false
    end  
    
    omgAreTesting = true
    white = tocolor(255,255,255,color.dxAlphaTest)
    imgcol = tocolor(255,255,255,color.imgAlphaTest)
    local test = {}
    test.name = name
    test.model = guiGetText(omgObjModel)
    test.points = point
    test.attaches = attach
    test.rotateOnly = guiCheckBoxGetSelected(omgObjRonly)
    test.noReturn = guiCheckBoxGetSelected(omgObjNoReturn)
    test.rox = guiGetText(omgObjSmrx)
    test.roy = guiGetText(omgObjSmry)
    test.roz = guiGetText(omgObjSmrz)
    test.mtime = guiGetText(omgObjRMtime)
    test.stime = guiGetText(omgObjRStime)
    guiSetText(omgObjTest, "STOP TEST")
    triggerServerEvent("omgDoObjectTest", getLocalPlayer(), test, getElementDimension(getLocalPlayer()), true)
  end
end

function omgHalt()
  if omgAreTesting then 
    triggerServerEvent("omgDoObjectTest", getLocalPlayer())
    omgAreTesting = false
    white = tocolor(255,255,255,color.dxAlpha)
    imgcol = tocolor(255,255,255,color.imgAlpha)
    guiSetText(omgObjTest, "Test movement")
    guiSetText(omgListTest, "Test Object")
  end  
end

function omgSaveObject()
  local err = false
  guiSetText(omgObjName, omgFormat(guiGetText(omgObjName)))
  if (#point == 0) then err = "No movement points added!" end
  if (#point == 1) and (not guiCheckBoxGetSelected(omgObjRonly)) then err = "You have only 1 point, add more or set object rotate-only!" end
  if (guiGetText(omgObjModel) == "") then err = "No object model specified!" end
  if err then
    omgError(err)
    return false
  end  
  
  local name = guiGetText(omgObjName)
  if not objs[name] then objs[name] = {} end
  objs[name].name = name
  objs[name].model = guiGetText(omgObjModel)
  objs[name].points = point
  objs[name].attaches = attach
  objs[name].rotateOnly = guiCheckBoxGetSelected(omgObjRonly)
  objs[name].noReturn = guiCheckBoxGetSelected(omgObjNoReturn)
  objs[name].rox = guiGetText(omgObjSmrx)
  objs[name].roy = guiGetText(omgObjSmry)
  objs[name].roz = guiGetText(omgObjSmrz)
  objs[name].mtime = guiGetText(omgObjRMtime)
  objs[name].stime = guiGetText(omgObjRStime)
  currentObject = objs[name].name
  return true
end

function omgMoveCamera()
  local name = gettok(guiGridListGetItemText(objList, guiGridListGetSelectedItem(objList), 1), 1, 58)
  if objs[name] and #objs[name].points > 0 then
    local x, y, z = objs[name].points[1].x, objs[name].points[1].y, objs[name].points[1].z
    setCameraMatrix(x, y, z+100, x, y, z)
  end
end

function omgObjAddPoint(pid)
  if (guiGetText(omgObjSx) == "") or (guiGetText(omgObjSy) == "") or (guiGetText(omgObjSz) == "") or (guiGetText(omgObjSrz) == "") or (guiGetText(omgObjSry) == "") or (guiGetText(omgObjSrx) == "") or (guiGetText(omgObjMtime) == "") or (guiGetText(omgObjStime) == "") then
    omgError("Not enough parameters to add a point!")
    return false
  end
  local data = {}
  data.x = guiGetText(omgObjSx)
  data.y = guiGetText(omgObjSy)
  data.z = guiGetText(omgObjSz)
  data.rx = guiGetText(omgObjSrx)
  data.ry = guiGetText(omgObjSry)
  data.rz = guiGetText(omgObjSrz)
  data.mt = guiGetText(omgObjMtime)
  data.st = guiGetText(omgObjStime)
  if pid and point[pid] then
    point[pid] = data
  else   
    table.insert(point, data) 
  end
end

function omgRelocateObject()
  local obj = exports["editor_main"]:getSelectedElement()
  if obj then
    local x,y,z = getElementPosition(obj)  
    if #point > 0 then
      local ox,oy,oz = point[1].x, point[1].y, point[1].z
      local offx = ox - x
      local offy = oy - y
      local offz = oz - z
      for i, p in ipairs(point) do
        point[i].x = point[i].x - offx
        point[i].y = point[i].y - offy
        point[i].z = point[i].z - offz
      end
    else    
      omgError("No starting point set!")  
    end  
  else
    omgError("Select object in place to relocate to!")  
  end
end

function omgDeleteLastPoint(pid)
  if #point > 0 and pid ~= 0 then 
    table.remove(point, pid)
    if #point < 1 then
      guiSetText(omgObjPadd, "Add (scroll: Edit)")
      guiSetText(omgObjPaddFrom, "Add new from object")
      selected = 0
    else
      if pid > #point then selected = #point end
      guiSetText(omgObjPadd, "Edit point: "..selected)
      guiSetText(omgObjPaddFrom, "Selected to point "..selected)
      guiSetText(omgObjSx, point[selected].x)
      guiSetText(omgObjSy, point[selected].y)
      guiSetText(omgObjSz, point[selected].z)
      guiSetText(omgObjSrx, point[selected].rx)
      guiSetText(omgObjSry, point[selected].ry)
      guiSetText(omgObjSrz, point[selected].rz)
      guiSetText(omgObjMtime, point[selected].mt)
      guiSetText(omgObjStime, point[selected].st)
    end
  end
end

function omgResetPoints()
  point = nil
  point = {}
  selected = 0
  guiSetText(omgObjPadd, "Add (scroll: Edit)")
  guiSetText(omgObjPaddFrom, "Add new from object")
end

function omgUpdateAttachCount()
  guiSetText(omgObjAttCount,"Objects attached: "..#attach)
end

function omgObjAddAttach()
  local obj = exports["editor_main"]:getSelectedElement()
  if obj then
    local id = getElementModel(obj)
    local x,y,z = getElementPosition(obj)  
    local rx,ry,rz = getElementRotation(obj)  
    local data = {}
    data.id = id
    data.rx = rx
    data.ry = ry
    data.rz = rz
    data.x = x
    data.y = y
    data.z = z
    table.insert(attach, data) 
    omgUpdateAttachCount()
  else
    omgError("No object selected!")  
  end
end

function omgResetAttaches()
  attach = nil
  attach = {}
  omgUpdateAttachCount()
end

function omgViewObject()
  local name = gettok(guiGridListGetItemText(objList, guiGridListGetSelectedItem(objList), 1), 1, string.byte(':'))
  if objs[name] then 
    currentObject = name 
  else 
    currentObject = nil
  end
end

function omgDoDelete()
  local name = gettok(guiGridListGetItemText(objList, guiGridListGetSelectedItem(objList), 1), 1, string.byte(':'))
  if objs[name] then
    if currentObject and (currentObject == name) then
      currentObject = nil
      objs[name] = nil
    end  
    omgUpdateList()
  end
end

function omgDoDeleteAll()
  if guiCheckBoxGetSelected(omgDisplay) then guiCheckBoxSetSelected(omgDisplay, false) end
  currentObject = nil
  objs = nil
  objs = {}
  omgUpdateList()
end

function omgDoEdit()
  local name = gettok(guiGridListGetItemText(objList, guiGridListGetSelectedItem(objList), 1), 1, string.byte(':'))
  if objs[name] then 
  	guiSetVisible(omgObjWindow, true)
    guiBringToFront(omgObjWindow)
    currentObject = name 
    guiSetText(omgObjName, name)
    guiSetText(omgObjModel, objs[name].model)
    point = objs[name].points
    attach = objs[name].attaches
    omgUpdateAttachCount()
    guiSetText(omgObjSmrx, objs[name].rox)
    guiSetText(omgObjSmry, objs[name].roy)
    guiSetText(omgObjSmrz, objs[name].roz)
    guiSetText(omgObjRStime, objs[name].stime)
    guiSetText(omgObjRMtime, objs[name].mtime)
    if objs[name].rotateOnly then guiCheckBoxSetSelected(omgObjRonly, true)
    else guiCheckBoxSetSelected(omgObjRonly, false) end
    if objs[name].noReturn then guiCheckBoxSetSelected(omgObjNoReturn, true)
    else guiCheckBoxSetSelected(omgObjNoReturn, false) end
  end
end

function omgUpdateList()
  guiGridListClear(objList)
  for i,object in pairs(objs) do
    local row = guiGridListAddRow(objList)
    local pn = "R"
    if not object.rotateOnly then pn = #object.points end
    guiGridListSetItemText(objList, row, objListName, object.name..":"..pn, false, false)
    guiGridListSetItemText(objList, row, objListModel, tostring(object.model), false, false)
  end
end

function omgUpdateXMLList()
  guiGridListClear(objXMLList)
  for i,node in ipairs(xmlNodeGetChildren(xml)) do
    local row = guiGridListAddRow(objXMLList)
    guiGridListSetItemText(objXMLList, row, objXMLListName, xmlNodeGetAttribute(node, "name"), false, false)
    guiGridListSetItemText(objXMLList, row, objXMLListModel, xmlNodeGetAttribute(node, "model"), false, false)
  end
end

function omgObjClear()
  guiSetText(omgObjName, "")
  guiSetText(omgObjModel, "")
  guiSetText(omgObjSx, "")
  guiSetText(omgObjSy, "")
  guiSetText(omgObjSz, "")
  guiSetText(omgObjSrx, "")
  guiSetText(omgObjSry, "")
  guiSetText(omgObjSrz, "")
  omgResetPoints()
  omgResetAttaches()
  guiSetText(omgObjSmrx, "0")
  guiSetText(omgObjSmry, "0")
  guiSetText(omgObjSmrz, "0")
  guiSetText(omgObjStime, "0")
  guiSetText(omgObjMtime, "5000")
  guiSetText(omgObjRStime, "0")
  guiSetText(omgObjRMtime, "5000")
  guiCheckBoxSetSelected(omgObjRonly, false)
  guiCheckBoxSetSelected(omgObjNoReturn, false)
end

function omgError(txt)
  guiSetText(omgErrorText, txt)
  guiSetVisible(omgErrorWindow, true)
  guiBringToFront(omgErrorWindow)
end

function omgDisableOthers(gui)
  local keyElements = {omgObjWindow,omgObjName,omgObjModel,omgObjSmrz,omgObjSmry,omgObjSmrx,omgObjStime,omgObjMtime,omgExportName,omgLuaResName,omgObjPadd}
  for i, gElement in ipairs(keyElements) do 
    if gui == gElement then return true end
  end
  return false
end

function renderPoints()
  fr = fr + 5
  if fr > 360 then fr = fr - 360 end
  if #point > 0 then
    local cx, cy, cz = getCameraMatrix(getLocalPlayer())
    local prevX, prevY, prevS = false, false, false
    local atime = false
    local origX, origY = getScreenFromWorldPosition(point[1].x, point[1].y, point[1].z, offScreen, true)
    local origDist = getDistanceBetweenPoints3D(cx, cy, cz, point[1].x, point[1].y, point[1].z)
    local origScale = dx.dist/origDist*dx.scale
    for i,pt in ipairs(point) do
      local ax, ay = getScreenFromWorldPosition(pt.x, pt.y, pt.z, offScreen, true)
      if not ax or not ay then break end
      local dist = getDistanceBetweenPoints3D(cx, cy, cz, pt.x, pt.y, pt.z)
      local scale = dx.dist/dist*dx.scale
      local sr = 0
      if i == selected then sr = fr end
      dxDrawImage(ax - 32*scale, ay - 32*scale, 64*scale,  64*scale, imgp, sr, 0, 0, imgcol)
      dxDrawText("point #"..i, ax+38*scale, ay-10*scale, 128*scale, 127*scale, white, 1.4*scale, "default-bold") 
      dxDrawText("STOP: " .. pt.st.. "ms", ax+38*scale, ay+10*scale, 128*scale, 127*scale, white, 0.7*scale, "default") 
      if prevX then
        dxDrawLine(ax, ay, prevX, prevY, white, 2)
        local mpx = (ax - prevX)/2
        local mpy = (ay - prevY)/2
        local midscale = (scale+prevS)/2
        local rot = findRotation(ax, ay, prevX, prevY)
        local arrowimg = imgarrow
        if (i == #point) and (#point == 2) and (not guiCheckBoxGetSelected(omgObjNoReturn)) then arrowimg = img end        
        dxDrawImage(prevX + mpx - 32*midscale, prevY + mpy - 32*midscale, 64*midscale, 64*midscale, arrowimg, rot, 0, 0, imgcol)
        dxDrawText(atime.."ms", prevX + mpx + 16*midscale, prevY + mpy - 11*midscale, 128*midscale, 27*midscale, white, 1*midscale, "default-bold") 
      end 
      if (i == #point) and (#point > 2) and (not guiCheckBoxGetSelected(omgObjNoReturn)) then 
        dxDrawLine(ax, ay, origX, origY, white, 2)
        local mpx = (ax - origX)/2
        local mpy = (ay - origY)/2
        local midscale = (scale+origScale)/2
        local rot = findRotation(origX, origY, ax, ay)
        dxDrawImage(origX + mpx - 32*midscale, origY + mpy - 32*midscale, 64*midscale, 64*midscale, imgarrow, rot, 0, 0, imgcol)
        dxDrawText(pt.mt.."ms", origX + mpx + 16*midscale, origY + mpy - 11*midscale, 128*midscale, 27*midscale, white, 1*midscale, "default-bold") 
      end
      prevX, prevY, prevS = ax, ay, scale
      atime = pt.mt
    end 
  elseif guiCheckBoxGetSelected(omgDisplay) then
    local cx, cy, cz = getCameraMatrix(getLocalPlayer())
    for i,object in pairs(objs) do
      local name = object.name
      local rrr = 0
      if objs[currentObject] and objs[currentObject].name == name then rrr = fr end
      if object.rotateOnly then
        local ax, ay = getScreenFromWorldPosition(object.points[1].x, object.points[1].y, object.points[1].z, offScreen, true)
        if not ax or not ay then break end
        local dist = getDistanceBetweenPoints3D(cx, cy, cz, object.points[1].x, object.points[1].y, object.points[1].z)
        local scale = dx.dist/dist*dx.scale
        local imgtype = 0
        if #object.attaches > 0 then imgtype = imgrattach
        else imgtype = imgr end
        dxDrawImage( ax - 32*scale, ay - 32*scale, 64*scale, 64*scale, imgtype, rrr, 0, 0, imgcol)
        dxDrawText(name .. ":R", ax+38*scale, ay-37*scale, 128*scale, 127*scale, white, 1.4*scale, "default-bold") 
        dxDrawText("X: " .. object.rox .. " degrees", ax+38*scale, ay-16*scale, 128*scale, 127*scale, white, 0.7*scale, "default") 
        dxDrawText("Y: " .. object.roy .. " degrees", ax+38*scale, ay-6*scale, 128*scale, 127*scale, white, 0.7*scale, "default") 
        dxDrawText("Z: " .. object.roz .. " degrees", ax+38*scale, ay+4*scale, 128*scale, 127*scale, white, 0.7*scale, "default")       
        dxDrawText("ROTATE: " .. object.mtime .. "ms, STOP: " .. object.stime .. "ms", ax+38*scale, ay+14*scale, 128*scale, 127*scale, white, 0.7*scale, "default")
      else
        local pts = object.points
        local prevX, prevY, prevS = false, false, false
        local atime = false
        local origX, origY = getScreenFromWorldPosition(pts[1].x, pts[1].y, pts[1].z, offScreen, true)
        local origDist = getDistanceBetweenPoints3D(cx, cy, cz, pts[1].x, pts[1].y, pts[1].z)
        local origScale = dx.dist/origDist*dx.scale
        for i,pt in ipairs(pts) do
          local ax, ay = getScreenFromWorldPosition(pt.x, pt.y, pt.z, offScreen, true)
          if not ax or not ay then break end
          local dist = getDistanceBetweenPoints3D(cx, cy, cz, pt.x, pt.y, pt.z)
          local scale = dx.dist/dist*dx.scale
          if i == 1 then 
            local imgtype = 0
            if #object.attaches > 0 then imgtype = imgattach
            else imgtype = imgs end
            dxDrawImage(ax - 32*scale, ay - 32*scale, 64*scale,  64*scale, imgtype, rrr, 0, 0, imgcol) 
          else 
            dxDrawImage(ax - 32*scale, ay - 32*scale, 64*scale, 64*scale, imgp, rrr, 0, 0, imgcol) 
          end
          dxDrawText(name .. ":"..i, ax+38*scale, ay-35*scale, 128*scale, 127*scale, white, 1.4*scale, "default-bold") 
          dxDrawText("X: " .. pt.x, ax+38*scale, ay-14*scale, 128*scale, 127*scale, white, 0.7*scale, "default") 
          dxDrawText("Y: " .. pt.y, ax+38*scale, ay-4*scale, 128*scale, 127*scale, white, 0.7*scale, "default") 
          dxDrawText("Z: " .. pt.z, ax+38*scale, ay+6*scale, 128*scale, 127*scale, white, 0.7*scale, "default")       
          dxDrawText("STOP: " .. pt.st .. "ms", ax+38*scale, ay+16*scale, 128*scale, 127*scale, white, 0.7*scale, "default")
          if prevX then
            dxDrawLine(ax, ay, prevX, prevY, white, 2)
            local mpx = (ax - prevX)/2
            local mpy = (ay - prevY)/2
            local midscale = (scale+prevS)/2
            local rot = findRotation(ax, ay, prevX, prevY)
            local arrowimg = imgarrow
            if (i == #pts) and (#pts == 2) and (not object.noReturn) then arrowimg = img end        
            dxDrawImage(prevX + mpx - 32*midscale, prevY + mpy - 32*midscale, 64*midscale, 64*midscale, arrowimg, rot, 0, 0, imgcol)
            dxDrawText(atime.."ms", prevX + mpx + 16*midscale, prevY + mpy - 11*midscale, 128*midscale, 27*midscale, white, 1*midscale, "default-bold") 
          end 
          if (i == #pts) and (#pts > 2) and (not object.noReturn) then 
            dxDrawLine(ax, ay, origX, origY, white, 2)
            local mpx = (ax - origX)/2
            local mpy = (ay - origY)/2
            local midscale = (scale+origScale)/2
            local rot = findRotation(origX, origY, ax, ay)
            dxDrawImage(origX + mpx - 32*midscale, origY + mpy - 32*midscale, 64*midscale, 64*midscale, imgarrow, rot, 0, 0, imgcol)
            dxDrawText(pt.mt.."ms", origX + mpx + 16*midscale, origY + mpy - 11*midscale, 128*midscale, 27*midscale, white, 1*midscale, "default-bold") 
          end
          prevX, prevY, prevS = ax, ay, scale
          atime = pt.mt
        end 
      end
    end  
  elseif currentObject then
    local name = objs[currentObject].name
    if not name then return false end
    local cx, cy, cz = getCameraMatrix(getLocalPlayer())
    if objs[name].rotateOnly then
      local ax, ay = getScreenFromWorldPosition(objs[name].points[1].x, objs[name].points[1].y, objs[name].points[1].z, offScreen, true)
      if not ax or not ay then return false end
      local dist = getDistanceBetweenPoints3D(cx, cy, cz, objs[name].points[1].x, objs[name].points[1].y, objs[name].points[1].z)
      local scale = dx.dist/dist*dx.scale
      local imgtype = 0
      if #objs[name].attaches > 0 then imgtype = imgrattach
      else imgtype = imgr end
      dxDrawImage(ax - 32*scale, ay - 32*scale, 64*scale,  64*scale, imgtype, 0, 0, 0, imgcol)
      dxDrawText(name .. ":R", ax+38*scale, ay-37*scale, 128*scale, 127*scale, white, 1.4*scale, "default-bold") 
      dxDrawText("X: " .. objs[name].rox .. " degrees", ax+38*scale, ay-16*scale, 128*scale, 127*scale, white, 0.7*scale, "default") 
      dxDrawText("Y: " .. objs[name].roy .. " degrees", ax+38*scale, ay-6*scale, 128*scale, 127*scale, white, 0.7*scale, "default") 
      dxDrawText("Z: " .. objs[name].roz .. " degrees", ax+38*scale, ay+4*scale, 128*scale, 127*scale, white, 0.7*scale, "default")       
      dxDrawText("ROTATE: " .. objs[name].mtime .. "ms, STOP: " .. objs[name].stime .. "ms", ax+38*scale, ay+14*scale, 128*scale, 127*scale, white, 0.7*scale, "default")
    else
      local pts = objs[name].points
      local prevX, prevY, prevS = false, false, false
      local atime = false
      local origX, origY = getScreenFromWorldPosition(pts[1].x, pts[1].y, pts[1].z, offScreen, true)
      local origDist = getDistanceBetweenPoints3D(cx, cy, cz, pts[1].x, pts[1].y, pts[1].z)
      local origScale = dx.dist/origDist*dx.scale
      for i,pt in ipairs(pts) do
        local ax, ay = getScreenFromWorldPosition(pt.x, pt.y, pt.z, offScreen, true)
        if not ax or not ay then break end
        local dist = getDistanceBetweenPoints3D(cx, cy, cz, pt.x, pt.y, pt.z)
        local scale = dx.dist/dist*dx.scale
        if i == 1 then 
          local imgtype = 0
          if #objs[name].attaches > 0 then imgtype = imgattach
          else imgtype = imgs end
          dxDrawImage(ax - 32*scale, ay - 32*scale, 64*scale,  64*scale, imgtype, 0, 0, 0, imgcol) 
        else 
          dxDrawImage(ax - 32*scale, ay - 32*scale, 64*scale,  64*scale, imgp, 0, 0, 0, imgcol) 
        end
        dxDrawText(name .. ":"..i, ax+38*scale, ay-35*scale, 128*scale, 127*scale, white, 1.4*scale, "default-bold") 
        dxDrawText("X: " .. pt.x, ax+38*scale, ay-14*scale, 128*scale, 127*scale, white, 0.7*scale, "default") 
        dxDrawText("Y: " .. pt.y, ax+38*scale, ay-4*scale, 128*scale, 127*scale, white, 0.7*scale, "default") 
        dxDrawText("Z: " .. pt.z, ax+38*scale, ay+6*scale, 128*scale, 127*scale, white, 0.7*scale, "default")       
        dxDrawText("STOP: " .. pt.st .. "ms", ax+38*scale, ay+16*scale, 128*scale, 127*scale, white, 0.7*scale, "default")
        if prevX then
          dxDrawLine(ax, ay, prevX, prevY, white, 2)
          local mpx = (ax - prevX)/2
          local mpy = (ay - prevY)/2
          local midscale = (scale+prevS)/2
          local rot = findRotation(ax, ay, prevX, prevY)
          local arrowimg = imgarrow
          if (i == #pts) and (#pts == 2) and (not objs[name].noReturn) then arrowimg = img end      
          dxDrawImage(prevX + mpx - 32*midscale, prevY + mpy - 32*midscale, 64*midscale, 64*midscale, arrowimg, rot, 0, 0, imgcol)
          dxDrawText(atime.."ms", prevX + mpx + 16*midscale, prevY + mpy - 11*midscale, 128*midscale, 27*midscale, white, 1*midscale, "default-bold") 
        end 
        if (i == #pts) and (#pts > 2) and (not objs[name].noReturn) then 
          dxDrawLine(ax, ay, origX, origY, white, 2)
          local mpx = (ax - origX)/2
          local mpy = (ay - origY)/2
          local midscale = (scale+origScale)/2
          local rot = findRotation(origX, origY, ax, ay)
          dxDrawImage(origX + mpx - 32*midscale, origY + mpy - 32*midscale, 64*midscale, 64*midscale, imgarrow, rot, 0, 0, imgcol)
          dxDrawText(pt.mt.."ms", origX + mpx + 16*midscale, origY + mpy - 11*midscale, 128*midscale, 27*midscale, white, 1*midscale, "default-bold") 
        end
        prevX, prevY, prevS = ax, ay, scale
        atime = pt.mt
      end 
    end
  else
    return false
  end  
end

function findRotation(x1,y1,x2,y2)
  local t = -math.deg(math.atan2(x2-x1,y2-y1))
  if t < 0 then t = t + 360 end
  return t
end

function omgRotate(a,b)
  local turn = b - a
  if turn < -180 then 
    turn = turn + 360
  elseif turn > 180 then  
    turn = turn - 360
  end
  return turn
end

function omgFileSave()
  local resource = guiGetText(omgLuaResName)
  local name = guiGetText(omgExportName)
  local code = omgDoExport()
  triggerServerEvent("omgLuaFileDump", getLocalPlayer(), resource, name, code)
end 

function omgDoExport()
  local scriptName = guiGetText(omgExportName)
  guiSetText(omgLuaWindow, "Script: "..scriptName..".lua")
  local o = objs
  -- points world rotation to movement relative rotation processing
  for i,ob in pairs(o) do
    local p = ob.points
    for j,pt in ipairs(p) do
      local ftime = o[i].points[#p].st
      if j == 1 then
        o[i].orix = pt.x
        o[i].oriy = pt.y
        o[i].oriz = pt.z
        o[i].orixr = pt.rx
        o[i].oriyr = pt.ry
        o[i].orizr = pt.rz
      end  
      if (j < #p) then
        local nextpt = j + 1
        o[i].points[j].dorx = omgRotate(pt.rx, o[i].points[nextpt].rx)
        o[i].points[j].dory = omgRotate(pt.ry, o[i].points[nextpt].ry)
        o[i].points[j].dorz = omgRotate(pt.rz, o[i].points[nextpt].rz)
      elseif j == #p then
        o[i].points[j].dorx = omgRotate(pt.rx, o[i].points[1].rx)
        o[i].points[j].dory = omgRotate(pt.ry, o[i].points[1].ry)
        o[i].points[j].dorz = omgRotate(pt.rz, o[i].points[1].rz)
      end
    end
  end
  local head = "-- DDC OMG generated script, PLACE IT SERVER-SIDE\n\n"
  
  -- OMG iterator creates function that creates objects
  head = head.."function omg_"..scriptName.."()\n"
  for i,object in pairs(o) do
    head = head.."  "..object.name.." = createObject("..object.model..", "..object.orix..", "..object.oriy..", "..object.oriz..", "..object.orixr..", "..object.oriyr..", "..object.orizr..")\n"
    for a, atc in ipairs(object.attaches) do
      local offX = atc.x - object.orix
      local offY = atc.y - object.oriy 
      local offZ = atc.z - object.oriz
      local rX = omgRotate(object.orixr, atc.rx) 
      local rY = omgRotate(object.oriyr, atc.ry) 
      local rZ = omgRotate(object.orizr, atc.rz) 
      local angle = -math.rad(object.orizr)
      local nX = offX*math.cos(angle) - offY*math.sin(angle)
      local nY = offY*math.cos(angle) + offX*math.sin(angle)
      head = head.."  "..object.name.."Attach"..a.." = createObject("..atc.id..", "..atc.x..", "..atc.y..", "..atc.z..", "..atc.rx..", "..atc.ry..", "..atc.rz..")\n"
      head = head.."  attachElements("..object.name.."Attach"..a..", "..object.name..", "..nX..", "..nY..", "..offZ..", "..rX..", "..rY..", "..rZ..")\n"
    end    
    head = head.."  omgMove"..object.name.."(1)\n"
  end
  head = head.."end\n\n"
  
  -- OMG iterator creates functions that move objects
  for i,object in pairs(o) do
    if object.rotateOnly then
      local x,y,z = object.orix, object.oriy, object.oriz
      local rx,ry,rz = object.rox, object.roy, object.roz
      head = head .. "function omgMove"..object.name.."(point)\n"
      head = head .. "    moveObject("..object.name..", "..object.mtime..", "..x..", "..y..", "..z..", "..rx..", "..ry..", "..rz..")\n"
      head = head .. "    setTimer(moveObject, "..object.mtime
      if tonumber(object.stime) > 0 then head = head .. " + "..object.stime end
      head = head .. ", 0, "..object.name..", "..object.mtime..", "..x..", "..y..", "..z..", "..rx..", "..ry..", "..rz..")\n"
      head = head .. "end\n\n"
    else
      head = head .. "function omgMove"..object.name.."(point)\n"
      local pts = object.points
      for i,pt in ipairs(object.points) do
        local nextpoint = i+1
        local nx,ny,nz = 0,0,0
        local nrx,nry,nrz = 0,0,0
        local stop = 0
        if i == #object.points then stop = object.points[1].st
        else stop = object.points[nextpoint].st end
        if i == #object.points then
          nx = object.points[1].x 
          ny = object.points[1].y 
          nz = object.points[1].z 
          nrx = object.points[1].rx 
          nry = object.points[1].ry 
          nrz = object.points[1].rz 
          nextpoint = 1
        else
          local ni = i+1
          nx = object.points[ni].x 
          ny = object.points[ni].y 
          nz = object.points[ni].z 
          nrx = object.points[ni].rx 
          nry = object.points[ni].ry 
          nrz = object.points[ni].rz 
        end
        if i == 1 then head = head .. "  if point == 1 then\n"
        else head = head .. "  elseif point == "..i.." then\n" end
        if (i == #object.points) and object.noReturn then
          head = head .. "    -- OBJECT STOP\n"
        else
          head = head .. "    moveObject("..object.name..", "..pt.mt..", "..nx..", "..ny..", "..nz..", "..pt.dorx..", "..pt.dory..", "..pt.dorz..")\n"
          head = head .. "    setTimer(omgMove"..object.name..", "..pt.mt
          if tonumber(stop) > 0 then head = head .. "+"..stop end
          head = head .. ", 1, "..nextpoint..")\n"
        end  
        if i == #object.points then head = head .. "  end\n" end  
      end  
      head = head .. "end\n\n"
    end
    
  end
   
  -- OMG last part - event handler to start this crap
  head = head.."addEventHandler(\"onResourceStart\", getResourceRootElement(getThisResource()), omg_"..scriptName..")\n"
  return head
end

function omgFormat(text, scriptname)
  if scriptname then
    local id = string.gsub(text, "%D", "");
    text = string.gsub(text, "%A", "");
    if text == "" then 
      text = "script" 
      id = math.random(10,99)
    end
    return text..id
  else 
    local id = string.gsub(text, "%D", "");
    text = string.gsub(text, "%A", "");
    if text == "" then 
      text = "omg" 
      id = math.random(1000,9999)
    end
    return text..id
  end  
end

function table.copy(t)
  local t2 = {}
  for k,v in pairs(t) do
    t2[k] = v
  end
  return t2
end

function xmlLoadObject(node)
  local data = {}
  for i,object in ipairs(xmlNodeGetChildren(xml)) do
    local name = xmlNodeGetAttribute(object, "name")
    if name == node then
      data.name = name
      data.model = tonumber(xmlNodeGetAttribute(object, "model"))
      data.rotateOnly = tobool(xmlNodeGetAttribute(object, "rotateOnly"))
      data.noReturn = tobool(xmlNodeGetAttribute(object, "noReturn"))
      data.rox = tonumber(xmlNodeGetAttribute(object, "rotX"))
      data.roy = tonumber(xmlNodeGetAttribute(object, "rotY"))
      data.roz = tonumber(xmlNodeGetAttribute(object, "rotZ"))
      data.mtime = tonumber(xmlNodeGetAttribute(object, "rotMove"))
      data.stime = tonumber(xmlNodeGetAttribute(object, "rotStop"))
      data.points = {}
      data.attaches = {}
      for i, n in ipairs(xmlNodeGetChildren(object)) do
        if xmlNodeGetName(n) == "point" then
          local id = tonumber(xmlNodeGetAttribute(n, "id")) 
          data.points[id] = {}
          data.points[id].x = tonumber(xmlNodeGetAttribute(n, "x")) 
          data.points[id].y = tonumber(xmlNodeGetAttribute(n, "y")) 
          data.points[id].z = tonumber(xmlNodeGetAttribute(n, "z")) 
          data.points[id].rx = tonumber(xmlNodeGetAttribute(n, "rx")) 
          data.points[id].ry = tonumber(xmlNodeGetAttribute(n, "ry")) 
          data.points[id].rz = tonumber(xmlNodeGetAttribute(n, "rz")) 
          data.points[id].mt = tonumber(xmlNodeGetAttribute(n, "move")) 
          data.points[id].st = tonumber(xmlNodeGetAttribute(n, "stop")) 
        elseif xmlNodeGetName(n) == "attach" then
          local id = tonumber(xmlNodeGetAttribute(n, "id")) 
          data.attaches[id] = {}
          data.attaches[id].id = tonumber(xmlNodeGetAttribute(n, "model")) 
          data.attaches[id].x = tonumber(xmlNodeGetAttribute(n, "x")) 
          data.attaches[id].y = tonumber(xmlNodeGetAttribute(n, "y")) 
          data.attaches[id].z = tonumber(xmlNodeGetAttribute(n, "z")) 
          data.attaches[id].rx = tonumber(xmlNodeGetAttribute(n, "rx")) 
          data.attaches[id].ry = tonumber(xmlNodeGetAttribute(n, "ry")) 
          data.attaches[id].rz = tonumber(xmlNodeGetAttribute(n, "rz")) 
        end
      end
      objs[name] = data
      omgUpdateList()
    end
  end
end

function xmlSaveObject(node)
  nodes = xmlNodeGetChildren(xml)
  for i,n in ipairs(nodes) do
    if xmlNodeGetAttribute(n, "name") == node then xmlDestroyNode(n) end
  end
  local objectNode = xmlCreateChild(xml, "object")
  xmlNodeSetAttribute(objectNode, "name", node)
  xmlNodeSetAttribute(objectNode, "model", tostring(objs[node].model))
  xmlNodeSetAttribute(objectNode, "rotateOnly", tostring(objs[node].rotateOnly))
  xmlNodeSetAttribute(objectNode, "noReturn", tostring(objs[node].noReturn))
  xmlNodeSetAttribute(objectNode, "rotX", tostring(objs[node].rox))
  xmlNodeSetAttribute(objectNode, "rotY", tostring(objs[node].roy))
  xmlNodeSetAttribute(objectNode, "rotZ", tostring(objs[node].roz))
  xmlNodeSetAttribute(objectNode, "rotMove", tostring(objs[node].mtime))
  xmlNodeSetAttribute(objectNode, "rotStop", tostring(objs[node].stime))
  for i, pt in ipairs(objs[node].points) do
    local pointNode = xmlCreateChild(objectNode, "point")
    xmlNodeSetAttribute(pointNode, "id", tostring(i))
    xmlNodeSetAttribute(pointNode, "x", tostring(pt.x))
    xmlNodeSetAttribute(pointNode, "y", tostring(pt.y))
    xmlNodeSetAttribute(pointNode, "z", tostring(pt.z))
    xmlNodeSetAttribute(pointNode, "rx", tostring(pt.rx))
    xmlNodeSetAttribute(pointNode, "ry", tostring(pt.ry))
    xmlNodeSetAttribute(pointNode, "rz", tostring(pt.rz))
    xmlNodeSetAttribute(pointNode, "move", tostring(pt.mt))
    xmlNodeSetAttribute(pointNode, "stop", tostring(pt.st))
  end
  for i, pt in ipairs(objs[node].attaches) do
    local pointNode = xmlCreateChild(objectNode, "attach")
    xmlNodeSetAttribute(pointNode, "id", tostring(i))
    xmlNodeSetAttribute(pointNode, "model", tostring(pt.id))
    xmlNodeSetAttribute(pointNode, "x", tostring(pt.x))
    xmlNodeSetAttribute(pointNode, "y", tostring(pt.y))
    xmlNodeSetAttribute(pointNode, "z", tostring(pt.z))
    xmlNodeSetAttribute(pointNode, "rx", tostring(pt.rx))
    xmlNodeSetAttribute(pointNode, "ry", tostring(pt.ry))
    xmlNodeSetAttribute(pointNode, "rz", tostring(pt.rz))
  end
  xmlSaveFile(xml)
end
    
function xmlDeleteObject(node)
  nodes = xmlNodeGetChildren(xml)
  for i,n in ipairs(nodes) do
    if xmlNodeGetAttribute(n, "name") == node then xmlDestroyNode(n) end
  end
  xmlSaveFile(xml)
end

addEventHandler("onClientResourceStop", resRoot,
	function()
  	xmlSaveFile(xml)
  	xmlUnloadFile(xml)
  end
)  

function tobool(v)
  if tostring(v) == "true" then return true 
  elseif tostring(v) == "false" then return false
  elseif tonumber(v) == 1 then return true
  elseif tonumber(v) == 0 then return false
  end
end