testObject = nil
rotTimer = nil
moveTimer = {}
testAttaches = {}

addEvent("omgLuaFileDump", true) 
addEventHandler("omgLuaFileDump", getRootElement(), 
  function(resource, luafile, code)
    if not resource or resource == "" then return false end
    if fileExists(":"..resource.."/"..luafile..".lua") then fileDelete(":"..resource.."/"..luafile..".lua") end  
    local file = fileCreate(":"..resource.."/"..luafile..".lua") 
    if not file then 
      outputChatBox("OMG: Failed creating a file in resources/"..resource.."/", getRootElement(), 250, 20, 50, true) 
      return false 
    end 
    fileWrite(file, code) 
    fileClose(file)
    outputChatBox("OMG: Lua saved: /"..resource.."/"..luafile..".lua by "..getPlayerName(source), getRootElement(), 50, 200, 50, true)
    local xml = xmlLoadFile(":"..resource.."/meta.xml")
    if not xml then 
      outputChatBox("OMG: meta.xml not found in resources/"..resource.."/", getRootElement(), 250, 20, 50, true) 
      return false 
    end
    local alreadyInMeta = false
    local metaNodes = xmlNodeGetChildren(xml) 
    for i,node in ipairs(metaNodes) do
      if xmlNodeGetName(node) == "script" and xmlNodeGetAttribute(node, "src") == luafile..".lua" then alreadyInMeta = true end
    end
    if not alreadyInMeta then 
      local scriptNode = xmlCreateChild(xml, "script")
      xmlNodeSetAttribute(scriptNode, "src", luafile..".lua")
      xmlNodeSetAttribute(scriptNode, "type", "server")
      xmlSaveFile(xml)
      outputChatBox("OMG: "..luafile..".lua added to: /"..resource.."/meta.xml by "..getPlayerName(source), getRootElement(), 50, 200, 50, true)
    end  
    xmlUnloadFile(xml)
  end
)

addEvent("omgDoObjectTest", true) 
addEventHandler("omgDoObjectTest", getRootElement(), 
function(object, dimension, start)
  if start then
    omgReset()
    testObject = object
    local p = object.points
    for j,pt in ipairs(p) do
      local ftime = object.points[#p].st
      if j == 1 then
        object.orix = pt.x
        object.oriy = pt.y
        object.oriz = pt.z
        object.orixr = pt.rx
        object.oriyr = pt.ry
        object.orizr = pt.rz
      end  
      if (j < #p) then
        local nextpt = j + 1
        object.points[j].dorx = omgRotate(pt.rx, object.points[nextpt].rx)
        object.points[j].dory = omgRotate(pt.ry, object.points[nextpt].ry)
        object.points[j].dorz = omgRotate(pt.rz, object.points[nextpt].rz)
      elseif j == #p then
        object.points[j].dorx = omgRotate(pt.rx, object.points[1].rx)
        object.points[j].dory = omgRotate(pt.ry, object.points[1].ry)
        object.points[j].dorz = omgRotate(pt.rz, object.points[1].rz)
      end
    end    
    testObject.name = createObject(object.model, object.orix, object.oriy, object.oriz, object.orixr, object.oriyr, object.orizr)
    setElementDimension(testObject.name, dimension)
    for a, atc in ipairs(object.attaches) do
      local offX = atc.x - object.orix
      local offY = atc.y - object.oriy 
      local offZ = atc.z - object.oriz
      local rX = omgRotate(object.orixr, atc.rx) 
      local rY = omgRotate(object.oriyr, atc.ry) 
      local rZ = omgRotate(object.orizr, atc.rz) 
      local angle = -math.rad(object.orizr)
      local nX = offX*math.cos(angle) - offY*math.sin(angle)
      local nY = offY*math.cos(angle) + offX*math.sin(angle)
      testAttaches[a] = createObject(atc.id, atc.x, atc.y, atc.z, atc.rx, atc.ry, atc.rz)
      setElementDimension(testAttaches[a], dimension)
      attachElements(testAttaches[a], testObject.name, nX, nY, offZ, rX, rY, rZ)
    end
    if object.rotateOnly then
      local x,y,z = object.orix, object.oriy, object.oriz
      local rx,ry,rz = object.rox, object.roy, object.roz
      moveObject(testObject.name, object.mtime, x, y, z, rx, ry, rz)
      rotTimer = setTimer(moveObject, object.mtime + object.stime, 0, testObject.name, object.mtime, x, y, z, rx, ry, rz)
    else
      moveToPoint(1)
    end
  else
    omgReset()
  end  
end
)

function omgReset()
  for i, timers in ipairs(moveTimer) do
    if isTimer(moveTimer[i]) then killTimer(moveTimer[i]) end
  end
  moveTimer = nil
  moveTimer = {}
  for j, attachs in ipairs(testAttaches) do
    if isElement(testAttaches[j]) then destroyElement(testAttaches[j]) end
  end
  testAttaches = nil
  testAttaches = {}
  if isTimer(rotTimer) then killTimer(rotTimer) end
  rotTimer = nil
  if testObject then destroyElement(testObject.name) end
  testObject = nil  
end

function moveToPoint(point)
  local object = testObject
  local pt = object.points[point]
  local nextpoint = point+1
  local nx,ny,nz = 0,0,0
  local nrx,nry,nrz = 0,0,0
  local stop = 0
  if point == #object.points then stop = object.points[1].st
  else stop = object.points[nextpoint].st end
  if point == #object.points then
    nx = object.points[1].x 
    ny = object.points[1].y 
    nz = object.points[1].z 
    nrx = object.points[1].rx 
    nry = object.points[1].ry 
    nrz = object.points[1].rz 
    nextpoint = 1
  else
    local ni = point+1
    nx = object.points[ni].x 
    ny = object.points[ni].y 
    nz = object.points[ni].z 
    nrx = object.points[ni].rx 
    nry = object.points[ni].ry 
    nrz = object.points[ni].rz 
  end  
  if (point == #object.points) and object.noReturn then
    return 
  else
    moveObject(testObject.name, pt.mt, nx, ny, nz, pt.dorx, pt.dory, pt.dorz)
    moveTimer[point] = setTimer(moveToPoint, pt.mt + stop, 1, nextpoint)
  end  
end

function getPD(x, y, dist, angle)
    local a = math.rad(90 - angle)
    local dx = math.cos(a) * dist
    local dy = math.sin(a) * dist
    return x+dx, y+dy
end

function omgRotate(a,b)
  local turn = b - a
  if turn < -180 then 
    turn = turn + 360
  elseif turn > 180 then  
    turn = turn - 360
  end
  return turn
end