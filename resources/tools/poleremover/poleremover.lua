local models = {1290,1306,1226,1615,1307,1308,1311,3855,1352,1351,1350,1315,1284,1283,1263,1262,3459,3854,3875,1231,1232,1294,1297,1298,1568,3460,3463,3472,3853,3513,3447,3337,3334,3335,3336,3410,1374,16448,16062}

addEventHandler("onClientResourceStart",resourceRoot,
	function()
		setCameraMatrix(3000,3000,3000)
		for _,model in pairs(models) do
			removeWorldModel(model, 50000, 0, 0, 0)
		end
		setTimer(setCameraTarget,1000,1,getLocalPlayer())
	end
)

addEventHandler("onClientResourceStop",resourceRoot,
	function()
		setCameraMatrix(3000,3000,3000)
		for _,model in pairs(models) do
			restoreWorldModel(model, 50000, 0, 0, 0)
		end
		setTimer(setCameraTarget,1000,1,getLocalPlayer())
	end
)