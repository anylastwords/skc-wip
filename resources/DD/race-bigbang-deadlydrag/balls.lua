-- DDC OMG generated script, PLACE IT SERVER-SIDE

function omg_balls()
  omg2452 = createObject(3252, 2878.4299316406, -1286.8000488281, 3.25, 0, 0, 0)
  omgMoveomg2452(1)
  omg9090 = createObject(3252, 2906.75, -1341.1800537109, 3.25, 0, 0, 0)
  omgMoveomg9090(1)
  omg2378 = createObject(3252, 2929.8798828125, -1286.8000488281, 3.25, 0, 0, 0)
  omgMoveomg2378(1)
  omg8781 = createObject(3252, 2881.5900878906, -1341.1800537109, 3.25, 0, 0, 0)
  omgMoveomg8781(1)
  omg8064 = createObject(3252, 2914.5400390625, -1286.8000488281, 3.25, 0, 0, 0)
  omgMoveomg8064(1)
  omg3022 = createObject(3252, 2835.1201171875, -1224.7199707031, 14.720000267029, 0, 0, 0)
  omgMoveomg3022(1)
  omg7275 = createObject(3252, 2897.9499511719, -1286.8000488281, 3.25, 0, 0, 0)
  omgMoveomg7275(1)
  omg5591 = createObject(3252, 2884.0900878906, -1341.1800537109, 3.25, 0, 0, 0)
  omgMoveomg5591(1)
end

function omgMoveomg2452(point)
  if point == 1 then
    moveObject(omg2452, 600, 2897.9499511719, -1286.8000488281, 3.25, 0, 0, 0)
    setTimer(omgMoveomg2452, 600, 1, 2)
  elseif point == 2 then
    moveObject(omg2452, 600, 2878.4299316406, -1286.8000488281, 3.25, 0, 0, 0)
    setTimer(omgMoveomg2452, 600, 1, 1)
  end
end

function omgMoveomg9090(point)
  if point == 1 then
    moveObject(omg9090, 1000, 2939.4799804688, -1341.1800537109, 3.25, 0, 0, 0)
    setTimer(omgMoveomg9090, 1000, 1, 2)
  elseif point == 2 then
    moveObject(omg9090, 1000, 2906.75, -1341.1800537109, 3.25, 0, 0, 0)
    setTimer(omgMoveomg9090, 1000, 1, 1)
  end
end

function omgMoveomg2378(point)
  if point == 1 then
    moveObject(omg2378, 600, 2914.5400390625, -1286.8000488281, 3.25, 0, 0, 0)
    setTimer(omgMoveomg2378, 600, 1, 2)
  elseif point == 2 then
    moveObject(omg2378, 600, 2929.8798828125, -1286.8000488281, 3.25, 0, 0, 0)
    setTimer(omgMoveomg2378, 600, 1, 1)
  end
end

function omgMoveomg8781(point)
  if point == 1 then
    moveObject(omg8781, 1000, 2854.6398925781, -1341.1800537109, 3.25, 0, 0, 0)
    setTimer(omgMoveomg8781, 1000, 1, 2)
  elseif point == 2 then
    moveObject(omg8781, 1000, 2881.5900878906, -1341.1800537109, 3.25, 0, 0, 0)
    setTimer(omgMoveomg8781, 1000, 1, 1)
  end
end

function omgMoveomg8064(point)
  if point == 1 then
    moveObject(omg8064, 600, 2878.4299316406, -1286.8000488281, 3.25, 0, 0, 0)
    setTimer(omgMoveomg8064, 600, 1, 2)
  elseif point == 2 then
    moveObject(omg8064, 600, 2914.5400390625, -1286.8000488281, 3.25, 0, 0, 0)
    setTimer(omgMoveomg8064, 600, 1, 1)
  end
end

function omgMoveomg3022(point)
  if point == 1 then
    moveObject(omg3022, 600, 2862.7099609375, -1224.7199707031, 14.720000267029, 0, 0, 0)
    setTimer(omgMoveomg3022, 600, 1, 2)
  elseif point == 2 then
    moveObject(omg3022, 600, 2835.1201171875, -1224.7199707031, 14.720000267029, 0, 0, 0)
    setTimer(omgMoveomg3022, 600, 1, 1)
  end
end

function omgMoveomg7275(point)
  if point == 1 then
    moveObject(omg7275, 600, 2929.8798828125, -1286.8000488281, 3.25, 0, 0, 0)
    setTimer(omgMoveomg7275, 600, 1, 2)
  elseif point == 2 then
    moveObject(omg7275, 600, 2897.9499511719, -1286.8000488281, 3.25, 0, 0, 0)
    setTimer(omgMoveomg7275, 600, 1, 1)
  end
end

function omgMoveomg5591(point)
  if point == 1 then
    moveObject(omg5591, 1000, 2906.75, -1341.1800537109, 3.25, 0, 0, 0)
    setTimer(omgMoveomg5591, 1000, 1, 2)
  elseif point == 2 then
    moveObject(omg5591, 1000, 2884.0900878906, -1341.1800537109, 3.25, 0, 0, 0)
    setTimer(omgMoveomg5591, 1000, 1, 1)
  end
end

addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), omg_balls)
