-- this shit is serverside, actually it all is

local x,y,z,radius = 2874.2099609375, -644.16998291016, 9.8199996948242, 8 -- le sphere location

local sphere = createColSphere(x,y,z,radius)

addEventHandler("onColShapeHit",sphere,
	function (hitElement,matchingDimension)
		if getElementType(hitElement) ~= "vehicle" then return end
		local player = getVehicleController(hitElement)
		if player then
			for i,p in ipairs (getElementsByType("player")) do
				if p ~= player then
					local v = getPedOccupiedVehicle(p)
					if v then
						blowVehicle(v,true)
					end
				end
			end
		end
	end
)