function startMusic()
    setRadioChannel(0)
    song = playSound("song.mp3",true)
    --setSoundVolume(song, 0.5)
	
end

function makeRadioStayOff()
    setRadioChannel(0)
    cancelEvent()
end

function toggleSong()
    if not songOff then
	    setSoundVolume(song,0)
		songOff = true
		removeEventHandler("onClientPlayerRadioSwitch",getRootElement(),makeRadioStayOff)
	else
	    setSoundVolume(song,1)
		songOff = false
		setRadioChannel(0)
		addEventHandler("onClientPlayerRadioSwitch",getRootElement(),makeRadioStayOff)
	end
end

addEventHandler("onClientResourceStart",getResourceRootElement(),
	function()
		outputChatBox("[Info] #FFA500Toggle music on/off using 'M'", 255, 255, 255, true)
		outputChatBox("[Info] #FFA500Touch the blue light to get a new vehicle", 255, 255, 255, true)
		startMusic()
	end
)
addEventHandler("onClientPlayerRadioSwitch",root,makeRadioStayOff)
addEventHandler("onClientPlayerVehicleEnter",root,makeRadioStayOff)
addCommandHandler("togglesong",toggleSong)
bindKey("m","down","togglesong")
