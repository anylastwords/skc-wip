local models = {
[1305] = { txd = "files/bird.txd", dff="files/bird.dff", col=false, lod=500 },
}

local song

addEventHandler("onClientResourceStart",root, 
	function()
		song = playSound("files/song.mp3",true)
		outputChatBox("Press m to mute the music")
		for modelId,modelData in pairs(models) do
			if ReplaceTexture(modelId,modelData.txd) then
				ReplaceModel(modelId,modelData)
			end
		end
	end 
)

bindKey("m","down",
	function ()
		if getSoundVolume(song) == 0 then
			setSoundVolume(song,1)
		else
			setSoundVolume(song,0)
		end
	end
)

function ReplaceTexture(modelId, texture)
	if texture then
		local txd = engineLoadTXD(texture)
		if not txd then
			outputConsole(texture.." couldn't be loaded")
		else
			return engineImportTXD(txd,modelId)
		end
	end
	return false
end


function ReplaceModel(modelId, modelData)
	if modelData.dff then
		local dff = engineLoadDFF(modelData.dff,0)
		if not dff then
			outputConsole(modelData.dff.." couldn't be loaded")
		else
			engineReplaceModel(dff,modelId)
		end
	end
	if modelData.col then
		local col = engineLoadCOL(modelData.col,modelId)
		if not col then
			outputConsole(modelData.col.." couldn't be loaded")
		else 
			engineReplaceCOL(col,modelId)
		end
	end 
	if modelData.lod then
		engineSetModelLODDistance(modelId,modelData.lod)
	end
	return false
end
