local loaded = 0

addEventHandler("onClientRender",root,
	function ()
		dxDrawText(tostring(loaded).." loaded!",500,500)
	end
)

addEventHandler("onClientResourceStart",resourceRoot,
	function ()
		for id,data in pairs (models) do
			engineSetModelLODDistance(id,300)
			replaceModel(id,data[1],data[2])
		end
	end
)

function replaceModel(id,dffFile,txdFile)
    local txd = engineLoadTXD("models/"..txdFile..".txd")
    if txd then
       engineImportTXD(txd,id)
	else
		--fileWrite(file,txdFile..".txd not found!\r\n")
		return
	end

    local col = engineLoadCOL("models/"..dffFile..".col")
	if col then
        engineReplaceCOL(col,id)
	else
		--fileWrite(file,dffFile..".col not found!\r\n")
		return
	end
	
    local dff = engineLoadDFF("models/"..dffFile..".dff",id)
	if dff then
		engineReplaceModel(dff,id)
	else
		--fileWrite(file,dffFile..".dff not found!\r\n")
		return
	end
	
	loaded = loaded+1
end