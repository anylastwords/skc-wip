function startMusic()
    setRadioChannel(0)
    song = playSound("edge.mp3",true)
	outputChatBox("Montgomery NFS, by [SKC]Dockinoke.")
	outputChatBox("Toggle music on/off using M")
	outputChatBox("Song: Metallingus - Edge theme")
end

function makeRadioStayOff()
    setRadioChannel(0)
    cancelEvent()
end

function toggleSong()
    if not songOff then
	    setSoundVolume(song,0)
		songOff = true
		removeEventHandler("onClientPlayerRadioSwitch",getRootElement(),makeRadioStayOff)
    outputChatBox("Problem")
	else
	    setSoundVolume(song,1)
		songOff = false
		setRadioChannel(0)
		addEventHandler("onClientPlayerRadioSwitch",getRootElement(),makeRadioStayOff)
    outputChatBox("GoodWay")
	end
end

addEventHandler("onClientResourceStart",getResourceRootElement(getThisResource()),startMusic)
addEventHandler("onClientPlayerRadioSwitch",getRootElement(),makeRadioStayOff)
addEventHandler("onClientPlayerVehicleEnter",getRootElement(),makeRadioStayOff)
addCommandHandler("mkmap1_racetheme",toggleSong)
bindKey("m","down","mkmap1_racetheme")
outputChatBox("Map made by [SKC]Dockinoke.")