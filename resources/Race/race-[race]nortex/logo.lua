﻿-- Skilled Drivers Rotating Logo by Nortex [SD~RL]
---- Copyright © 2012 Nortex
---- Declaring this script as yours is shit, because I spent my time into it.
-- Version 0.3 (Alpha & Velocity Addition)

-- Variables
local screenX, screenY = guiGetScreenSize()
local currentRotation = 0
local alpha = 0
local reverse = false

-- Functions
function sdrl_ResourceStart( theresource )
	addEventHandler("onClientRender", getRootElement(), sdrl_ResourceRender)
end

function sdrl_Alpha( )
	if reverse then
		alpha = alpha - 2.5
		if alpha <= 0 then reverse = false end
	else
		alpha = alpha + 2.5
		if alpha >= 255 then reverse = true end
	end
end

function sdrl_ResourceRender( )
	local vehicle = getPedOccupiedVehicle(localPlayer)
	if not vehicle then
		dxDrawImage( screenX - 300, 30, 80, 80, "logo/logo.bmp", currentRotation, 0, 0, tocolor(255, 255, 255, alpha), true )
		currentRotation = currentRotation + 1
		sdrl_Alpha( )
		return
	else
		local x, y, z = getElementVelocity(vehicle)
		dxDrawImage( screenX - 300, 30, 80, 80, "logo/logo.bmp", currentRotation, 0, 0, tocolor(255, 255, 255, alpha), true )
		local sqrtX, sqrtY = math.sqrt(x*x), math.sqrt(y*y)
		currentRotation = currentRotation + sqrtX + sqrtY + ((sqrtX + sqrtY) / 2)
		sdrl_Alpha( )
	end
end

-- Events
addEventHandler("onClientResourceStart", resourceRoot, sdrl_ResourceStart)