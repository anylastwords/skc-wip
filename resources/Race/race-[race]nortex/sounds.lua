local audioSource = nil
local audioState = false

function onEnableAudio()
    setRadioChannel( 0 )
    audioSource = playSound( "sounds/music.mp3", true )
end

function onDisableRadio()
    setRadioChannel( 0 )
    cancelEvent()
end

function onToggleAudio()
    if not audioState then
		audioState = true
	    setSoundVolume( audioSource, 0 )
		removeEventHandler( "onClientPlayerRadioSwitch", root, onDisableRadio )
	else
		audioState = false
		setRadioChannel( 0 )
	    setSoundVolume( audioSource, 1 )
		addEventHandler( "onClientPlayerRadioSwitch", root, onDisableRadio )
	end
end

addEventHandler( 'onClientResourceStart', getResourceRootElement(getThisResource()),
	function( resource )
		--' Other '--
		setWaterColor( 255, 0, 0 )
		-- setSkyGradient( 0, 0, 0, 0, 0, 0 )
		--' Message '--
		outputChatBox("#CCCCCC*[Map]* This map has been created by #99FF00Nortex", 255, 255, 255, true);
		outputChatBox("#CCCCCC*[Music]* #99FF00Time To Say Goodbye", 255, 255, 255, true);
		outputChatBox("#CCCCCC*[Music]* Toggle music using the key '#99FF00m#CCCCCC', Script from #99FF00Nortex", 255, 255, 255, true);
	end
)

bindKey( "m", "down", onToggleAudio )
addEventHandler( "onClientResourceStart", resourceRoot, onEnableAudio )
addEventHandler( "onClientPlayerRadioSwitch", root, onDisableRadio )
addEventHandler( "onClientPlayerVehicleEnter", root, onDisableRadio )