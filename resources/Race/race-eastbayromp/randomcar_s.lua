
	--carsids:
	--1st line: 2-Door and Compact cars (no Bravura 401, Fortune 526, Cadrona 527, Stafford 580, Esperanto 419, Picador 600, Picador 585, Virgo 491, Previon 436)
	--2th line: 4-Door and Luxury cars (no Primo 547, Manana 410, Oceanic 467, Greenwood 492, Intruder 546, Willard 529, Glendale 466, Glendale Damaged 604, Washington 421, Tahoma 566, Vincent 540)
	--3th line: Bikes (no Bike 509, BMX 481, Faggio 462, FCR-900 521, BF-400 581, Mountain Bike 510, Pizzaboy 448, Wayfarer 586)
	--4th line: Civil Servant / Public Transportation (no Baggage 485, Sweeper 574, Trashmaster 408, Cabbie 438, Utility Van 552, Towtruck 525, Taxi 420, Bus 431)
	--5th line: Government Vehicles (no Rhino 432, Barracks 433, S.W.A.T 601, HPV1000 523, Police LV 598, Police LS 596, Fire Truck Ladder 544, Ambulance 416, Securicar 428)
	--6th line: Heavy and Utility Trucks (no Combine Harvester 532, Dozer 486, Dumper 406, Linerunner (From "Tanker Commando") 514, Tractor 531, Yankee 456, Mule 414, Mr. Whoopee 423, Boxville 498, Boxville Mission 609, Hotdog 588, Linerunner 403, Benson 499, Packer 443, Cement Truck 524, Hotdog 588, Dune 573)
	--7th line: Light Trucks and Vans (no Forklift 530, Mower 572, Tug 583, Walton 478, Pony 413, Moonbeam 418, Berkley's RC Van 459, Rumpo 440, Yosemite 554, Sadler 543, Damaged Sadler 605, News Van 582)
	--8th line: Lowriders (all)
	--9th line: Muscle Cars (all)
	--10th line: Recreational (no Caddy 457, BF Injection 424, Vortex 539, Journey 508, Quadbike 471, Kart 571, Camper 483, Monster 1 444, Monster 2 556)
	--11th line: Street Racers (no Stratum 561)
	--12th line: SUVs and Wagons (no Rancher (From "Lure") 505, Regina 479, Rancher 489, Romero 442, Perennial 404)
	local carids = 	{602, 496, 518, 589, 533, 474, 545, 517, 439, 549,
			445, 507, 587, 551, 516, 426, 405, 409, 550,
			463, 522, 461, 468,
			437,
			427, 490, 528, 407, 470, 597, 599,
			578, 455, 515,
			422, 482,
			536, 575, 534, 567, 535, 576, 412,
			402, 542, 603, 475,
			568, 504, 500, 557, 495,
			429, 541, 415, 480, 562, 565, 434, 494, 502, 503, 411, 559, 560, 506, 451, 555, 477,
			579, 400, 458}

	--boatids: (no Predator 430, Reefer 453, Tropic 454, Marquis 484)
	local boatids = {472, 473, 493, 595, 452, 446}

	--planeids: (no Andromeda 592, AT-400 577, Cargobob 548, Hunter 425, Leviathan 417, Maverick 487, Nevada 553, News Chopper 488, Police Maverick 497, Raindance 563, Seasparrow 447, Sparrow 467)
	local planeids = {511, 512, 593, 476, 519, 460, 513}

    function randomCar(checkpoint, time)
        local veh = getPedOccupiedVehicle(source)

        if checkpoint == 2 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 3 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 4 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 5 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 6 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 7 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 8 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 9 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 10 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 11 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 12 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 13 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 14 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 15 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 16 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
             setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 17 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
             setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 18 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
             setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 19 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
             setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 20 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 21 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 22 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 23 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 24 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 25 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

	if checkpoint == 25 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

	if checkpoint == 26 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

	if checkpoint == 27 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

	if checkpoint == 28 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

	if checkpoint == 29 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

	if checkpoint == 30 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

	if checkpoint == 31 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

             if checkpoint == 32 then
	    triggerClientEvent (source, "startclientzmodifier", getRootElement())
            setElementModel (veh, carids[math.random(#carids)])
	end

    end
    addEvent('onPlayerReachCheckpoint')
    addEventHandler('onPlayerReachCheckpoint', getRootElement(), randomCar)
