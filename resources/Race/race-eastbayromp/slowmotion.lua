----------------
--SlowMotion01--
----------------

slowmotioncol01 = createColPolygon (0, 0, -1417.1, 53.6, -1439.1, 75.5, -1482.7, 31.9, -1460.7, 10.1)

function slowmotion01on (theElement, matchingDimensions)
	if theElement == getLocalPlayer() then

		setGameSpeed (tonumber (0.3))
	end
end
addEventHandler("onClientColShapeHit", slowmotioncol01, slowmotion01on)

function slowmotion01off (theElement, matchingDimensions)
	if theElement == getLocalPlayer() then

		setGameSpeed (tonumber (1.05))
	end
end
addEventHandler("onClientColShapeLeave", slowmotioncol01, slowmotion01off)


function startFinalCPSpeedboost ()

	thePlayer = getLocalPlayer()
	theVehicle = getPedOccupiedVehicle (thePlayer)
	vx, vy, vz = getElementVelocity (theVehicle)
	setElementVelocity (theVehicle, vx * -1.75, vy * -1.75, vz)

end
addEvent("onClientPlayerFinish")
addEventHandler ("onClientPlayerFinish", getRootElement(), startFinalCPSpeedboost)

--setDevelopmentMode (enable)