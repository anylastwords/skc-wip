thePlayer = getLocalPlayer ()

function zmodifier ()
	theVehicle = getPedOccupiedVehicle (thePlayer)
	x, y, z = getElementPosition (theVehicle)
	setElementPosition (theVehicle, x, y, z+0.75)
	setVehicleLocked (theVehicle, true)
	setVehicleDoorsUndamageable (theVehicle, true)
	setTimer (function ()
		setVehiclePanelState (theVehicle, 0, 0)
		setVehiclePanelState (theVehicle, 1, 0)
		setVehiclePanelState (theVehicle, 2, 0)
		setVehiclePanelState (theVehicle, 3, 0)
		setVehiclePanelState (theVehicle, 4, 0)
		setVehiclePanelState (theVehicle, 5, 0)
		setVehiclePanelState (theVehicle, 6, 0)
	end, 750, 1)
end
addEvent( "startclientzmodifier", true )
addEventHandler( "startclientzmodifier", getRootElement(), zmodifier)


function planezmodifier ()
	theVehicle = getPedOccupiedVehicle (thePlayer)
	x, y, z = getElementPosition (theVehicle)
	setElementPosition (theVehicle, x, y, z+3)
	setVehicleLocked (theVehicle, true)
	setVehicleDoorsUndamageable (theVehicle, true)
	setTimer (function ()
		setVehiclePanelState (theVehicle, 0, 0)
		setVehiclePanelState (theVehicle, 1, 0)
		setVehiclePanelState (theVehicle, 2, 0)
		setVehiclePanelState (theVehicle, 3, 0)
		setVehiclePanelState (theVehicle, 4, 0)
		setVehiclePanelState (theVehicle, 5, 0)
		setVehiclePanelState (theVehicle, 6, 0)
	end, 750, 1)
end
addEvent( "startclientplanezmodifier", true )
addEventHandler( "startclientplanezmodifier", getRootElement(), planezmodifier)


----------------
--Vehicle Name--
----------------

local screenWidth, screenHeight = guiGetScreenSize()

function vehName ()
	theVehicle = getPedOccupiedVehicle (thePlayer)
	if theVehicle then
	vehType = getVehicleType (theVehicle)
	if vehType == "Bike" or vehType == "BMX" or vehType == "Quad" then											--if player's vehicle is a bike
		vehicleName = getVehicleName (theVehicle)
		dxDrawText ('Bike: ', 0, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
		dxDrawText (vehicleName, 60, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
	elseif vehType == "Automobile" then																		--if player's vehicle is a car
		vehicleName = getVehicleName (theVehicle)
		dxDrawText ('Car: ', 4, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
		dxDrawText (vehicleName, 60, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
	elseif vehType == "Plane" or vehType == "Helicopter" then													--if player's vehicle is a plane
		vehicleName = getVehicleName (theVehicle)
		dxDrawText ('Plane: ', 0, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
		dxDrawText (vehicleName, 75, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
	elseif vehType == "Boat" then																				--if player's vehicle is a boat
		vehicleName = getVehicleName (theVehicle)
		dxDrawText ('Boat: ', 0, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
		dxDrawText (vehicleName, 65, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
	end
	end
end
addEventHandler("onClientRender", getRootElement(), vehName)
