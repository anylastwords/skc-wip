addEvent ("onPlayerReachCheckpoint")
addEventHandler ("onPlayerReachCheckpoint", getRootElement(),
	function (cp, time)
		if cp == 1 then
			local vehicle = getPedOccupiedVehicle (source)
			local vx, vy, vz = getElementVelocity (vehicle)
			setElementVelocity (vehicle, vx * 1.9, vy * 1.9, vz)
			playSoundFrontEnd (source, 18)
		end

		if cp == 32 then
			local vehicle = getPedOccupiedVehicle (source)
			local vx, vy, vz = getElementVelocity (vehicle)
			setElementVelocity (vehicle, vx * 1.75, vy * 1.75, vz)
			playSoundFrontEnd (source, 18)
		end
	end
)