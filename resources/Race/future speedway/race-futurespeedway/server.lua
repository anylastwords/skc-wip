--1
	obj01 = createObject (2381, 5993.943359375, -615.3525390625, 76, 0, 270, 270)
	obj02 = createObject (2381, 5956.8935546875, -615.3525390625, 53, 0, 270, 270)

	function move1_1 ()
		moveObject (obj01, 4000, 5993.943359375, -615.3525390625, 53, 0, 0, 0)
		moveObject (obj02, 4000, 5956.8935546875, -615.3525390625, 76, 0, 0, 0)
		setTimer (move1_2, 4100, 1)
	end

	function move1_2 ()
		moveObject (obj01, 4000, 5993.943359375, -615.3525390625, 76, 0, 0, 0)
		moveObject (obj02, 4000, 5956.8935546875, -615.3525390625, 53, 0, 0, 0)
		setTimer (move1_1, 4100, 1)
	end

--2
	obj03 = createObject (2381, 5993.9580078125, -910.4443359375, 31.655511856079, 0, 0, 0)
	obj04 = createObject (2381, 5956.8251953125, -910.439453125, 61.755512237549, 0, 0, 0)

	function move2_1 ()
		moveObject (obj04, 4000, 5956.8251953125, -910.439453125, 31.655511856079, 0, 0, 0)
		setTimer (move2_2, 4100, 1)
	end

	function move2_2 ()
		moveObject (obj03, 4000, 5993.9580078125, -910.4443359375, 61.755512237549, 0, 0, 0)
		setTimer (move2_3, 4100, 1)
	end

	function move2_3 ()
		moveObject (obj03, 4000, 5993.9580078125, -910.4443359375, 31.655511856079, 0, 0, 0)
		setTimer (move2_4, 4100, 1)
	end

	function move2_4 ()
		moveObject (obj04, 4000, 5956.8251953125, -910.439453125, 61.755512237549, 0, 0, 0)
		setTimer (move2_1, 4100, 1)
	end

--3
	obj05 = createObject (2381, 5993.9580078125, -1385.5999755859, 50.799999237061, 0, 270, 270)

	function move3_1 ()
		moveObject (obj05, 3000, 5956.8251953125, -1385.5999755859, 50.799999237061, 0, 0, 0)
		setTimer (move3_2, 3100, 1)
	end

	function move3_2 ()
		moveObject (obj05, 3000, 5993.9580078125, -1385.5999755859, 50.799999237061, 0, 0, 0)
		setTimer (move3_1, 3100, 1)
	end

--4
	obj06 = createObject (1337, 5995.3999023438, -1827.087890625, 70, 0, 0, 0)
	setElementAlpha (obj06, 0)
	obj06a = createObject (2384, 5975.3999023438, -1827.087890625, 70, 0, 0, 0)
	obj06b = createObject (2384, 5975.3999023438, -1827.087890625, 70, 0, 0, 0)
	obj06c = createObject (2384, 5975.3999023438, -1827.087890625, 70, 0, 0, 0)
	obj06d = createObject (2384, 5975.3999023438, -1827.087890625, 70, 0, 0, 0)
	obj06e = createObject (2384, 5975.3999023438, -1827.087890625, 70, 0, 0, 0)
	obj06f = createObject (2384, 5975.3999023438, -1827.087890625, 70, 0, 0, 0)
		attachElements (obj06a, obj06, 0, 0, -35, 0, 0, 0)
		attachElements (obj06b, obj06, 0, 0, 35, 0, 0, 0)
		attachElements (obj06c, obj06, -35, 0, 0, 0, 90, 0)
		attachElements (obj06d, obj06, 35, 0, 0, 0, 90, 0)
		attachElements (obj06e, obj06, 0, 0, 0, 0, 45, 0)
		attachElements (obj06f, obj06, 0, 0, 0, 0, -45, 0)

	obj07 = createObject (1337, 5955.3999023438, -1975.2470703125, 70, 0, 0, 0)
	setElementAlpha (obj07, 0)
	obj07a = createObject (2384, 5955.3999023438, -1975.2470703125, 70, 0, 0, 0)
	obj07b = createObject (2384, 5955.3999023438, -1975.2470703125, 70, 0, 0, 0)
	obj07c = createObject (2384, 5955.3999023438, -1975.2470703125, 70, 0, 0, 0)
	obj07d = createObject (2384, 5955.3999023438, -1975.2470703125, 70, 0, 0, 0)
	obj07e = createObject (2384, 5955.3999023438, -1975.2470703125, 70, 0, 0, 0)
	obj07f = createObject (2384, 5955.3999023438, -1975.2470703125, 70, 0, 0, 0)
		attachElements (obj07a, obj07, 0, 0, -35, 0, 0, 0)
		attachElements (obj07b, obj07, 0, 0, 35, 0, 0, 0)
		attachElements (obj07c, obj07, -35, 0, 0, 0, 90, 0)
		attachElements (obj07d, obj07, 35, 0, 0, 0, 90, 0)
		attachElements (obj07e, obj07, 0, 0, 0, 0, 45, 0)
		attachElements (obj07f, obj07, 0, 0, 0, 0, -45, 0)

	obj08 = createObject (1337, 5995.3999023438, -2123.40625, 70, 0, 0, 0)
	setElementAlpha (obj08, 0)
	obj08a = createObject (2384, 5995.3999023438, -2123.40625, 70, 0, 0, 0)
	obj08b = createObject (2384, 5995.3999023438, -2123.40625, 70, 0, 0, 0)
	obj08c = createObject (2384, 5995.3999023438, -2123.40625, 70, 0, 0, 0)
	obj08d = createObject (2384, 5995.3999023438, -2123.40625, 70, 0, 0, 0)
	obj08e = createObject (2384, 5995.3999023438, -2123.40625, 70, 0, 0, 0)
	obj08f = createObject (2384, 5995.3999023438, -2123.40625, 70, 0, 0, 0)
		attachElements (obj08a, obj08, 0, 0, -35, 0, 0, 0)
		attachElements (obj08b, obj08, 0, 0, 35, 0, 0, 0)
		attachElements (obj08c, obj08, -35, 0, 0, 0, 90, 0)
		attachElements (obj08d, obj08, 35, 0, 0, 0, 90, 0)
		attachElements (obj08e, obj08, 0, 0, 0, 0, 45, 0)
		attachElements (obj08f, obj08, 0, 0, 0, 0, -45, 0)

	obj09 = createObject (1337, 5955.3999023438, -2272.0634765625, 70, 0, 0, 0)
	setElementAlpha (obj09, 0)
	obj09a = createObject (2384, 5955.3999023438, -2272.0634765625, 70, 0, 0, 0)
	obj09b = createObject (2384, 5955.3999023438, -2272.0634765625, 70, 0, 0, 0)
	obj09c = createObject (2384, 5955.3999023438, -2272.0634765625, 70, 0, 0, 0)
	obj09d = createObject (2384, 5955.3999023438, -2272.0634765625, 70, 0, 0, 0)
	obj09e = createObject (2384, 5955.3999023438, -2272.0634765625, 70, 0, 0, 0)
	obj09f = createObject (2384, 5955.3999023438, -2272.0634765625, 70, 0, 0, 0)
		attachElements (obj09a, obj09, 0, 0, -35, 0, 0, 0)
		attachElements (obj09b, obj09, 0, 0, 35, 0, 0, 0)
		attachElements (obj09c, obj09, -35, 0, 0, 0, 90, 0)
		attachElements (obj09d, obj09, 35, 0, 0, 0, 90, 0)
		attachElements (obj09e, obj09, 0, 0, 0, 0, 45, 0)
		attachElements (obj09f, obj09, 0, 0, 0, 0, -45, 0)

	obj10 = createObject (1337, 5995.3999023438, -2420.35546875, 70, 0, 0, 0)
	setElementAlpha (obj10, 0)
	obj10a = createObject (2384, 5995.3999023438, -2420.35546875, 70, 0, 0, 0)
	obj10b = createObject (2384, 5995.3999023438, -2420.35546875, 70, 0, 0, 0)
	obj10c = createObject (2384, 5995.3999023438, -2420.35546875, 70, 0, 0, 0)
	obj10d = createObject (2384, 5995.3999023438, -2420.35546875, 70, 0, 0, 0)
	obj10e = createObject (2384, 5995.3999023438, -2420.35546875, 70, 0, 0, 0)
	obj10f = createObject (2384, 5995.3999023438, -2420.35546875, 70, 0, 0, 0)
		attachElements (obj10a, obj10, 0, 0, -35, 0, 0, 0)
		attachElements (obj10b, obj10, 0, 0, 35, 0, 0, 0)
		attachElements (obj10c, obj10, -35, 0, 0, 0, 90, 0)
		attachElements (obj10d, obj10, 35, 0, 0, 0, 90, 0)
		attachElements (obj10e, obj10, 0, 0, 0, 0, 45, 0)
		attachElements (obj10f, obj10, 0, 0, 0, 0, -45, 0)

	obj11 = createObject (1337, 5955.3999023438, -2568.6474609375, 70, 0, 0, 0)
	setElementAlpha (obj11, 0)
	obj11a = createObject (2384, 5955.3999023438, -2568.6474609375, 70, 0, 0, 0)
	obj11b = createObject (2384, 5955.3999023438, -2568.6474609375, 70, 0, 0, 0)
	obj11c = createObject (2384, 5955.3999023438, -2568.6474609375, 70, 0, 0, 0)
	obj11d = createObject (2384, 5955.3999023438, -2568.6474609375, 70, 0, 0, 0)
	obj11e = createObject (2384, 5955.3999023438, -2568.6474609375, 70, 0, 0, 0)
	obj11f = createObject (2384, 5955.3999023438, -2568.6474609375, 70, 0, 0, 0)
		attachElements (obj11a, obj11, 0, 0, -35, 0, 0, 0)
		attachElements (obj11b, obj11, 0, 0, 35, 0, 0, 0)
		attachElements (obj11c, obj11, -35, 0, 0, 0, 90, 0)
		attachElements (obj11d, obj11, 35, 0, 0, 0, 90, 0)
		attachElements (obj11e, obj11, 0, 0, 0, 0, 45, 0)
		attachElements (obj11f, obj11, 0, 0, 0, 0, -45, 0)

	function move4_1 ()
		moveObject (obj06, 7200, 5955.3999023438, -1827.087890625, 70, 0, 720, 0)
		moveObject (obj07, 7200, 5995.3999023438, -1975.2470703125, 70, 0, 720, 0)
		moveObject (obj08, 7200, 5955.3999023438, -2123.40625, 70, 0, 720, 0)
		moveObject (obj09, 7200, 5995.3999023438, -2272.0634765625, 70, 0, 720, 0)
		moveObject (obj10, 7200, 5955.3999023438, -2420.35546875, 70, 0, 720, 0)
		moveObject (obj11, 7200, 5995.3999023438, -2568.6474609375, 70, 0, 720, 0)
		setTimer (move4_2, 7300, 1)
	end
	
	function move4_2 ()
		moveObject (obj06, 7200, 5995.3999023438, -1827.087890625, 70, 0, 720, 0)
		moveObject (obj07, 7200, 5955.3999023438, -1975.2470703125, 70, 0, 720, 0)
		moveObject (obj08, 7200, 5995.3999023438, -2123.40625, 70, 0, 720, 0)
		moveObject (obj09, 7200, 5955.3999023438, -2272.0634765625, 70, 0, 720, 0)
		moveObject (obj10, 7200, 5995.3999023438, -2420.35546875, 70, 0, 720, 0)
		moveObject (obj11, 7200, 5955.3999023438, -2568.6474609375, 70, 0, 720, 0)
		setTimer (move4_1, 7300, 1)
	end

--5
	obj12 = createObject (2373, 5624.615234375, -1679.8134765625, 33.708667755127, 0, 0, 0)
	obj13 = createObject (2373, 5652.087890625, -1235.0517578125, 33.708667755127, 0, 0, 0)
	obj14 = createObject (2372, 5624.515625, -923.2705078125, 34.02649307251, 0, 0, 0)

	function move5_1 ()
		moveObject (obj12, 10000, 5652.087890625, -1679.8134765625, 33.708667755127, 0, 0, 0)
		moveObject (obj13, 10000, 5624.615234375, -1235.0517578125, 33.708667755127, 0, 0, 0)
		moveObject (obj14, 10000, 5651.8125, -923.2705078125, 34.02649307251, 0, 0, 0)
		setTimer (move5_2, 10100, 1)
	end

	function move5_2 ()
		moveObject (obj12, 10000, 5624.615234375, -1679.8134765625, 33.708667755127, 0, 0, 0)
		moveObject (obj13, 10000, 5652.087890625, -1235.0517578125, 33.708667755127, 0, 0, 0)
		moveObject (obj14, 10000, 5624.515625, -923.2705078125, 34.02649307251, 0, 0, 0)
		setTimer (move5_1, 10100, 1)
	end
	


function startmovingobjects ()

	move1_1 ()
	move2_1 ()
	move3_1 ()
	move4_1 ()
	move5_1 ()

end
addEventHandler ("onResourceStart", resourceRoot, startmovingobjects)

--------------------------
--------------------------
--			--
--	SETTINGS	--
--			--
--------------------------
--------------------------

elementSync = true		--possible options: true or false

setElementSyncer (obj01, elementSync)
setElementSyncer (obj02, elementSync)

setElementSyncer (obj03, elementSync)
setElementSyncer (obj04, elementSync)

setElementSyncer (obj05, elementSync)

setElementSyncer (obj06, elementSync)
setElementSyncer (obj06a, elementSync)
setElementSyncer (obj06b, elementSync)
setElementSyncer (obj06c, elementSync)
setElementSyncer (obj06d, elementSync)
setElementSyncer (obj06e, elementSync)
setElementSyncer (obj06f, elementSync)
setElementSyncer (obj07, elementSync)
setElementSyncer (obj07a, elementSync)
setElementSyncer (obj07b, elementSync)
setElementSyncer (obj07c, elementSync)
setElementSyncer (obj07d, elementSync)
setElementSyncer (obj07e, elementSync)
setElementSyncer (obj07f, elementSync)
setElementSyncer (obj08, elementSync)
setElementSyncer (obj08a, elementSync)
setElementSyncer (obj08b, elementSync)
setElementSyncer (obj08c, elementSync)
setElementSyncer (obj08d, elementSync)
setElementSyncer (obj08e, elementSync)
setElementSyncer (obj08f, elementSync)
setElementSyncer (obj09, elementSync)
setElementSyncer (obj09a, elementSync)
setElementSyncer (obj09b, elementSync)
setElementSyncer (obj09c, elementSync)
setElementSyncer (obj09d, elementSync)
setElementSyncer (obj09e, elementSync)
setElementSyncer (obj09f, elementSync)
setElementSyncer (obj10, elementSync)
setElementSyncer (obj10a, elementSync)
setElementSyncer (obj10b, elementSync)
setElementSyncer (obj10c, elementSync)
setElementSyncer (obj10d, elementSync)
setElementSyncer (obj10e, elementSync)
setElementSyncer (obj10f, elementSync)
setElementSyncer (obj11, elementSync)
setElementSyncer (obj11a, elementSync)
setElementSyncer (obj11b, elementSync)
setElementSyncer (obj11c, elementSync)
setElementSyncer (obj11d, elementSync)
setElementSyncer (obj11e, elementSync)
setElementSyncer (obj11f, elementSync)

setElementSyncer (obj12, elementSync)
setElementSyncer (obj13, elementSync)
setElementSyncer (obj14, elementSync)
