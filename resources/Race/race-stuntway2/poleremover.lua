local models = {1283, 1294, 615, 1307, 620, 730, 1226, 733, 1297, 1461, 1231, 1280, 1312, 1315, 1290, }

addEventHandler("onClientResourceStart",resourceRoot,
	function()
		setCameraMatrix(3000,3000,3000)
		for _,model in pairs(models) do
			removeWorldModel(model, 50000, 0, 0, 0)
		end
		setTimer(setCameraTarget,1000,1,getLocalPlayer())
	end
)

addEventHandler("onClientResourceStop",resourceRoot,
	function()
		setCameraMatrix(3000,3000,3000)
		for _,model in pairs(models) do
			restoreWorldModel(model, 50000, 0, 0, 0)
		end
		setTimer(setCameraTarget,1000,1,getLocalPlayer())
	end
)