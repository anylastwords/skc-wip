
local models = {


[7076] = { txd = "textures.txd", dff = "tube50mglass.dff",        col = "tube50mglass.col",        lod=2000 },
[7075] = { txd = "textures.txd", dff = "tube50mglass180bend.dff", col = "tube50mglass180bend.col", lod=2000 },
[5783] = { txd = "textures.txd", dff = "tube50mglass45bend.dff",  col = "tube50mglass45bend.col",  lod=2000 },
[5764] = { txd = "textures.txd", dff = "tube50mglass90bend.dff",  col = "tube50mglass90bend.col",  lod=2000 },
[5681] = { txd = "textures.txd", dff = "tube50mglassbulge.dff",   col = "tube50mglassbulge.col",   lod=2000 },
[5337] = { txd = "textures.txd", dff = "tube50mglassfunnel.dff",  col = "tube50mglassfunnel.col",  lod=2000 },
[5326] = { txd = "textures.txd", dff = "tube50mglassplus.dff",    col = "tube50mglassplus.col",    lod=2000 },
[5323] = { txd = "textures.txd", dff = "tube50mglasst.dff",       col = "tube50mglasst.col",       lod=2000 }


}


function ReplaceTexture(modelId, texture)
	if texture then
		local txd = engineLoadTXD(texture)
		if not txd then
			outputConsole(texture .." couldn't be loaded")
		else
			return engineImportTXD(txd, modelId)
		end
	end
	return false
end


function ReplaceModel(modelId, modelData)
	if modelData.dff then
		local dff = engineLoadDFF(modelData.dff, 0)
		if not dff then
			outputConsole(modelData.dff .." couldn't be loaded")
		else
			engineReplaceModel(dff, modelId)
		end
	end
	if modelData.col then
		local col = engineLoadCOL(modelData.col, modelId)
		if not col then
			outputConsole(modelData.col .." couldn't be loaded")
		else	
			engineReplaceCOL(col, modelId)
		end
	end	
	if modelData.lod then
		engineSetModelLODDistance(modelId, modelData.lod)
	end
end


addEventHandler("onClientResourceStart", getResourceRootElement(), 
	function()
		for modelId,modelData in pairs(models) do
			if ReplaceTexture(modelId, modelData.txd) then
				ReplaceModel(modelId, modelData)
			end
		end
	end 
)