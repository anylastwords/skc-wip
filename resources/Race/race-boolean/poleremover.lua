local models = {1351, 1350, 1297, 1232, 1231, 1226, 1283, 1568, 1284,  737, 705, 738, 792, 731, 669, 708, 672,  1412, 1468, 1312, 1375, 1300, 717, 1257, 1290, 9526, 1229, 1233, 1223, 1211}

addEventHandler("onClientResourceStart",resourceRoot,
	function()
		setCameraMatrix(3000,3000,3000)
		for _,model in pairs(models) do
			removeWorldModel(model, 50000, 0, 0, 0)
		end
		setTimer(setCameraTarget,1000,1,getLocalPlayer())
	end
)

addEventHandler("onClientResourceStop",resourceRoot,
	function()
		setCameraMatrix(3000,3000,3000)
		for _,model in pairs(models) do
			restoreWorldModel(model, 50000, 0, 0, 0)
		end
		setTimer(setCameraTarget,1000,1,getLocalPlayer())
	end
)