local models = {

[4000] = { txd = "bld.txd", dff="bld_009.dff", col="bld_009.col", lod=2000 },
[4001] = { txd = "bld.txd", dff="bld_001.dff", col="bld_001.col", lod=2000 },
[4002] = { txd = "bld.txd", dff="bld_002.dff", col="bld_002.col", lod=2000 },
[4003] = { txd = "bld.txd", dff="bld_007.dff", col="bld_007.col", lod=2000 },
[4004] = { txd = "trackc.txd", dff="track_068.dff", col="track_068.col", lod=2000 },
[4005] = { txd = "bld.txd", dff="bld_006.dff", col="bld_006.col", lod=2000 },
[4006] = { txd = "bld.txd", dff="bld_004.dff", col="bld_004.col", lod=2000 },
[4007] = { txd = "bld.txd", dff="bld_005.dff", col="bld_005.col", lod=2000 },
[4008] = { txd = "bld.txd", dff="bld_014.dff", col="bld_014.col", lod=2000 },
[4009] = { txd = "trackc.txd", dff="track_011.dff", col="track_011.col", lod=2000 },
[4010] = { txd = "trackc.txd", dff="track_066.dff", col="track_066.col", lod=2000 },
[4011] = { txd = "trackc.txd", dff="track_065.dff", col="track_065.col", lod=2000 },
[4012] = { txd = "trackc.txd", dff="track_067.dff", col="track_067.col", lod=2000 },
[4013] = { txd = "bld.txd", dff="bld_015.dff", col="bld_015.col", lod=2000 },
[4014] = { txd = "trackc.txd", dff="track_001.dff", col="track_001.col", lod=2000 },
[4015] = { txd = "trackc.txd", dff="track_002.dff", col="track_002.col", lod=2000 },
[4016] = { txd = "trackc.txd", dff="track_003.dff", col="track_003.col", lod=2000 },
[4017] = { txd = "trackc.txd", dff="track_004.dff", col="track_004.col", lod=2000 },
[4018] = { txd = "trackc.txd", dff="track_005.dff", col="track_005.col", lod=2000 },
[4019] = { txd = "trackc.txd", dff="track_006.dff", col="track_006.col", lod=2000 },
[4020] = { txd = "trackc.txd", dff="track_007.dff", col="track_007.col", lod=2000 },
[4021] = { txd = "trackc.txd", dff="track_008.dff", col="track_008.col", lod=2000 },
[4022] = { txd = "trackc.txd", dff="track_009.dff", col="track_009.col", lod=2000 },
[4023] = { txd = "trackc.txd", dff="track_010.dff", col="track_010.col", lod=2000 },
[4024] = { txd = "trackc.txd", dff="track_012.dff", col="track_012.col", lod=2000 },
[4025] = { txd = "trackc.txd", dff="track_013.dff", col="track_013.col", lod=2000 },
[4026] = { txd = "trackc.txd", dff="track_014.dff", col="track_014.col", lod=2000 },
[4027] = { txd = "trackc.txd", dff="track_015.dff", col="track_015.col", lod=2000 },
[4028] = { txd = "trackc.txd", dff="track_016.dff", col="track_016.col", lod=2000 },
[4029] = { txd = "trackc.txd", dff="track_017.dff", col="track_017.col", lod=2000 },
[4030] = { txd = "trackc.txd", dff="track_018.dff", col="track_018.col", lod=2000 },
[4031] = { txd = "trackc.txd", dff="track_019.dff", col="track_019.col", lod=2000 },
[4032] = { txd = "trackc.txd", dff="track_020.dff", col="track_020.col", lod=2000 },
[4033] = { txd = "trackc.txd", dff="track_021.dff", col="track_021.col", lod=2000 },
[4034] = { txd = "trackc.txd", dff="track_022.dff", col="track_022.col", lod=2000 },
[4035] = { txd = "trackc.txd", dff="track_023.dff", col="track_023.col", lod=2000 },
[4036] = { txd = "trackc.txd", dff="track_024.dff", col="track_024.col", lod=2000 },
[4037] = { txd = "trackc.txd", dff="track_025.dff", col="track_025.col", lod=2000 },
[4038] = { txd = "trackc.txd", dff="track_026.dff", col="track_026.col", lod=2000 },
[4039] = { txd = "trackc.txd", dff="track_027.dff", col="track_027.col", lod=2000 },
[4040] = { txd = "trackc.txd", dff="track_028.dff", col="track_028.col", lod=2000 },
[4041] = { txd = "trackc.txd", dff="track_029.dff", col="track_029.col", lod=2000 },
[4042] = { txd = "trackc.txd", dff="track_030.dff", col="track_030.col", lod=2000 },
[4043] = { txd = "trackc.txd", dff="track_031.dff", col="track_031.col", lod=2000 },
[4044] = { txd = "trackc.txd", dff="track_032.dff", col="track_032.col", lod=2000 },
[4045] = { txd = "trackc.txd", dff="track_033.dff", col="track_033.col", lod=2000 },
[4046] = { txd = "trackc.txd", dff="track_034.dff", col="track_034.col", lod=2000 },
[4047] = { txd = "trackc.txd", dff="track_035.dff", col="track_035.col", lod=2000 },
[4048] = { txd = "trackc.txd", dff="track_036.dff", col="track_036.col", lod=2000 },
[4049] = { txd = "trackc.txd", dff="track_037.dff", col="track_037.col", lod=2000 },
[4050] = { txd = "trackc.txd", dff="track_038.dff", col="track_038.col", lod=2000 },
[4051] = { txd = "trackc.txd", dff="track_039.dff", col="track_039.col", lod=2000 },
[4052] = { txd = "trackc.txd", dff="track_040.dff", col="track_040.col", lod=2000 },
[4053] = { txd = "trackc.txd", dff="track_041.dff", col="track_041.col", lod=2000 },
[4054] = { txd = "trackc.txd", dff="track_042.dff", col="track_042.col", lod=2000 },
[4055] = { txd = "trackc.txd", dff="track_043.dff", col="track_043.col", lod=2000 },
[4056] = { txd = "trackc.txd", dff="track_044.dff", col="track_044.col", lod=2000 },
[4057] = { txd = "trackc.txd", dff="track_045.dff", col="track_045.col", lod=2000 },
[4058] = { txd = "trackc.txd", dff="track_047.dff", col="track_047.col", lod=2000 },
[4059] = { txd = "trackc.txd", dff="track_048.dff", col="track_048.col", lod=2000 },
[4060] = { txd = "trackc.txd", dff="track_049.dff", col="track_049.col", lod=2000 },
[4061] = { txd = "trackc.txd", dff="track_050.dff", col="track_050.col", lod=2000 },
[4062] = { txd = "trackc.txd", dff="track_051.dff", col="track_051.col", lod=2000 },
[4063] = { txd = "trackc.txd", dff="track_052.dff", col="track_052.col", lod=2000 },
[4064] = { txd = "trackc.txd", dff="track_053.dff", col="track_053.col", lod=2000 },
[4065] = { txd = "trackc.txd", dff="track_054.dff", col="track_054.col", lod=2000 },
[4066] = { txd = "trackc.txd", dff="track_055.dff", col="track_055.col", lod=2000 },
[4067] = { txd = "trackc.txd", dff="track_056.dff", col="track_056.col", lod=2000 },
[4068] = { txd = "trackc.txd", dff="track_057.dff", col="track_057.col", lod=2000 },
[4069] = { txd = "trackc.txd", dff="track_058.dff", col="track_058.col", lod=2000 },
[4070] = { txd = "trackc.txd", dff="track_059.dff", col="track_059.col", lod=2000 },
[4071] = { txd = "trackc.txd", dff="track_060.dff", col="track_060.col", lod=2000 },
[4072] = { txd = "trackc.txd", dff="track_061.dff", col="track_061.col", lod=2000 },
[4073] = { txd = "trackc.txd", dff="track_062.dff", col="track_062.col", lod=2000 },
[4074] = { txd = "trackc.txd", dff="track_063.dff", col="track_063.col", lod=2000 },
[4075] = { txd = "trackc.txd", dff="track_064.dff", col="track_064.col", lod=2000 },
[4076] = { txd = "bld.txd", dff="bld_003.dff", col="bld_003.col", lod=2000 },
[4077] = { txd = "bld.txd", dff="bld_013.dff", col="bld_013.col", lod=2000 },
[4078] = { txd = "bld.txd", dff="bld_008.dff", col="bld_008.col", lod=2000 },
[4079] = { txd = "bld.txd", dff="bld_010.dff", col="bld_010.col", lod=2000 },
[4080] = { txd = "bld.txd", dff="bld_011.dff", col="bld_011.col", lod=2000 },
[4081] = { txd = "bld.txd", dff="bld_012.dff", col="bld_012.col", lod=2000 },
[4082] = { txd = "trackc.txd", dff="track_046.dff", col="track_046.col", lod=2000 },




}

function ReplaceTexture(modelId, texture)
 if texture then
  local txd = engineLoadTXD(texture)
  if not txd then
   outputConsole(texture .." couldn't be loaded")
  else
   return engineImportTXD(txd, modelId)
  end
 end
 return false
end


function ReplaceModel(modelId, modelData)
 if modelData.dff then
  local dff = engineLoadDFF(modelData.dff, 0)
  if not dff then
   outputConsole(modelData.dff .." couldn't be loaded")
  else
   engineReplaceModel(dff, modelId)
  end
 end
 if modelData.col then
  local col = engineLoadCOL(modelData.col, modelId)
  if not col then
   outputConsole(modelData.col .." couldn't be loaded")
  else 
   engineReplaceCOL(col, modelId)
  end
 end 
 if modelData.lod then
  engineSetModelLODDistance(modelId, modelData.lod)
 end
end


addEventHandler("onClientResourceStart", getResourceRootElement(), 
 function()
  for modelId,modelData in pairs(models) do
   if ReplaceTexture(modelId, modelData.txd) then
    ReplaceModel(modelId, modelData)
   end
  end
 end 
)
