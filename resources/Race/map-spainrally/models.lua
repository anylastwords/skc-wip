local models = {

[4001] = { txd = "spainrally.txd", dff="spainrally01.dff", col="spainrally01.col", lod=2000 },
[4002] = { txd = "spainrally.txd", dff="spainrally02.dff", col="spainrally02.col", lod=2000 },
[4003] = { txd = "spainrally.txd", dff="spainrally03.dff", col="spainrally03.col", lod=2000 },
[4004] = { txd = "spainrally.txd", dff="spainrally04.dff", col="spainrally04.col", lod=2000 },
[4005] = { txd = "spainrally.txd", dff="spainrally05.dff", col="spainrally05.col", lod=2000 },
[4006] = { txd = "spainrally.txd", dff="spainrally06.dff", col="spainrally06.col", lod=2000 },
[4007] = { txd = "spainrally.txd", dff="spainrally07.dff", col="spainrally07.col", lod=2000 },
[4008] = { txd = "spainrally.txd", dff="spainrally08.dff", col="spainrally08.col", lod=2000 },
[4010] = { txd = "spainrally.txd", dff="spainrally09.dff", col="spainrally09.col", lod=2000 },
[4011] = { txd = "spainrally.txd", dff="spainrally10.dff", col="spainrally10.col", lod=2000 },


}


function ReplaceTexture(modelId, texture)
	if texture then
		local txd = engineLoadTXD(texture)
		if not txd then
			outputConsole(texture .." couldn't be loaded")
		else
			return engineImportTXD(txd, modelId)
		end
	end
	return false
end


function ReplaceModel(modelId, modelData)
	if modelData.dff then
		local dff = engineLoadDFF(modelData.dff, 0)
		if not dff then
			outputConsole(modelData.dff .." couldn't be loaded")
		else
			engineReplaceModel(dff, modelId)
		end
	end
	if modelData.col then
		local col = engineLoadCOL(modelData.col, modelId)
		if not col then
			outputConsole(modelData.col .." couldn't be loaded")
		else	
			engineReplaceCOL(col, modelId)
		end
	end	
	if modelData.lod then
		engineSetModelLODDistance(modelId, modelData.lod)
	end
end


addEventHandler("onClientResourceStart", getResourceRootElement(), 
	function()
		for modelId,modelData in pairs(models) do
			if ReplaceTexture(modelId, modelData.txd) then
				ReplaceModel(modelId, modelData)
			end
		end
	end 
)