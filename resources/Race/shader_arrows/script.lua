addEventHandler("onClientResourceStart",resourceRoot,
	function ()

		dff = engineLoadDFF ('ammuwindows_sfs.dff', 0) 
		engineReplaceModel (dff, 10632)
		dff = engineLoadDFF ('carshowwin_sfsx.dff', 0) 
		engineReplaceModel (dff, 3851)

	end
)

addEventHandler("onClientResourceStart",resourceRoot,
	function()
		local shader = dxCreateShader ("shader.fx")

		if not shader then
			return outputChatBox ( "Could not create shader. Please use debugscript 3" )
		end
		
		number = {1, 2, 3, 4, 5}
		number = number[math.random (#number)]
		local texture = dxCreateTexture ("images/image"..number..".png","dxt5")
		dxSetShaderValue (shader, "Tex", texture)
		engineApplyShaderToWorldTexture (shader, "ws_ammu-posh")
		engineSetModelLODDistance (10632, 100)
		engineApplyShaderToWorldTexture (shader, "ws_carshowwin1")
		engineSetModelLODDistance (3851, 100)
	end
)