actioncol01 = createColSphere (-849, 2221.8999023438, 81.300003051758, 15)

function action01 (theElement)
	if theElement == localPlayer then
		setElementCollisionsEnabled(getElementByID("rocket2"),false) -- collisions off so they don't explode when projectile is created
		setElementCollisionsEnabled(getElementByID("base2"),false)
		--local velX, velY, velZ = getElementVelocity(getPedOccupiedVehicle(localPlayer)) -- so projectile has enough speed
		--outputChatBox(velX.."x".. velY.."x".. velZ) -- this is used to find out the speed needed for a projectile
		
		createProjectile(localPlayer, 20, -850.8, 2221.9, 82.7, 1, getElementByID("target2"), nil, nil, nil, 1.125, -0.035, -2.75e-006) -- put the 3 values here, rounded for better looks :P
		-- creator = localPlayer so it doesn't sync for every1 just the one making the rocket (dunno how the explosion would behave tho)
		-- projectile number 20 coz that's the heat seeking rocket
		-- get the starting coords from the .map file for the projectile
		-- force 1 is enough
		-- getElementByID("target2") to get the target for the projectile; use the name in the .map file under id=""
	end
end
addEventHandler("onClientColShapeHit", actioncol01, action01)
