addEventHandler('onClientResourceStart', resourceRoot, 
function() 

	txd = engineLoadTXD('files/2.txd')
	engineImportTXD(txd, 2052)
	engineImportTXD(txd, 2053)
	engineImportTXD(txd, 2054)
	engineImportTXD(txd, 2371)
	engineImportTXD(txd, 2372)
	engineImportTXD(txd, 2373)
	engineImportTXD(txd, 2374)
	engineImportTXD(txd, 2377)
	engineImportTXD(txd, 2378)
	engineImportTXD(txd, 2380)
	engineImportTXD(txd, 2381)
	engineImportTXD(txd, 2382)
	engineImportTXD(txd, 2383)
	engineImportTXD(txd, 2384)
	engineImportTXD(txd, 2386)
	engineImportTXD(txd, 2389)
	engineImportTXD(txd, 2390)
	engineImportTXD(txd, 2391)
	engineImportTXD(txd, 2392)
	engineImportTXD(txd, 2394)

	local col = engineLoadCOL('files/1.col') 
	engineReplaceCOL(col, 2052)
	local col = engineLoadCOL('files/2.col') 
	engineReplaceCOL(col, 2053)
	local col = engineLoadCOL('files/3.col') 
	engineReplaceCOL(col, 2054)
	local col = engineLoadCOL('files/4.col') 
	engineReplaceCOL(col, 2371)
	local col = engineLoadCOL('files/5.col') 
	engineReplaceCOL(col, 2372)
	local col = engineLoadCOL('files/6.col') 
	engineReplaceCOL(col, 2373)
	local col = engineLoadCOL('files/7.col') 
	engineReplaceCOL(col, 2374)
	local col = engineLoadCOL('files/8.col') 
	engineReplaceCOL(col, 2377)
	local col = engineLoadCOL('files/9.col') 
	engineReplaceCOL(col, 2378)
	local col = engineLoadCOL('files/10.col') 
	engineReplaceCOL(col, 2380)
	local col = engineLoadCOL('files/11.col') 
	engineReplaceCOL(col, 2381)
	local col = engineLoadCOL('files/12.col') 
	engineReplaceCOL(col, 2382)
	local col = engineLoadCOL('files/13.col') 
	engineReplaceCOL(col, 2383)
	local col = engineLoadCOL('files/14.col') 
	engineReplaceCOL(col, 2384)
	local col = engineLoadCOL('files/15.col') 
	engineReplaceCOL(col, 2386)
	local col = engineLoadCOL('files/16.col') 
	engineReplaceCOL(col, 2389)
	local col = engineLoadCOL('files/17.col') 
	engineReplaceCOL(col, 2390)
	local col = engineLoadCOL('files/18.col') 
	engineReplaceCOL(col, 2391)
	local col = engineLoadCOL('files/19.col') 
	engineReplaceCOL(col, 2392)
	local col = engineLoadCOL('files/20.col') 
	engineReplaceCOL(col, 2394)

	local dff = engineLoadDFF('files/1.dff', 0) 
	engineReplaceModel(dff, 2052)  
	local dff = engineLoadDFF('files/2.dff', 0) 
	engineReplaceModel(dff, 2053)
        local dff = engineLoadDFF('files/3.dff', 0) 
	engineReplaceModel(dff, 2054)  
	local dff = engineLoadDFF('files/4.dff', 0) 
	engineReplaceModel(dff, 2371)
        local dff = engineLoadDFF('files/5.dff', 0) 
	engineReplaceModel(dff, 2372)
        local dff = engineLoadDFF('files/6.dff', 0) 
	engineReplaceModel(dff, 2373)  
	local dff = engineLoadDFF('files/7.dff', 0) 
	engineReplaceModel(dff, 2374)
        local dff = engineLoadDFF('files/8.dff', 0) 
	engineReplaceModel(dff, 2377)  
	local dff = engineLoadDFF('files/9.dff', 0) 
	engineReplaceModel(dff, 2378)
        local dff = engineLoadDFF('files/10.dff', 0) 
	engineReplaceModel(dff, 2380)
        local dff = engineLoadDFF('files/11.dff', 0) 
	engineReplaceModel(dff, 2381)  
	local dff = engineLoadDFF('files/12.dff', 0) 
	engineReplaceModel(dff, 2382)
        local dff = engineLoadDFF('files/13.dff', 0) 
	engineReplaceModel(dff, 2383)  
	local dff = engineLoadDFF('files/14.dff', 0) 
	engineReplaceModel(dff, 2384)
        local dff = engineLoadDFF('files/15.dff', 0) 
	engineReplaceModel(dff, 2386)
        local dff = engineLoadDFF('files/16.dff', 0) 
	engineReplaceModel(dff, 2389)  
	local dff = engineLoadDFF('files/17.dff', 0) 
	engineReplaceModel(dff, 2390)
        local dff = engineLoadDFF('files/18.dff', 0) 
	engineReplaceModel(dff, 2391)  
	local dff = engineLoadDFF('files/19.dff', 0) 
	engineReplaceModel(dff, 2392)
        local dff = engineLoadDFF('files/20.dff', 0) 
	engineReplaceModel(dff, 2394)
 	
	engineSetModelLODDistance(2052, 500)
	engineSetModelLODDistance(2053, 500)
	engineSetModelLODDistance(2054, 500)
	engineSetModelLODDistance(2371, 500)
	engineSetModelLODDistance(2372, 500)
	engineSetModelLODDistance(2373, 500)
	engineSetModelLODDistance(2374, 500)
	engineSetModelLODDistance(2377, 500)
	engineSetModelLODDistance(2378, 500)
	engineSetModelLODDistance(2380, 500)
	engineSetModelLODDistance(2381, 500)
	engineSetModelLODDistance(2382, 500)
	engineSetModelLODDistance(2383, 500)
	engineSetModelLODDistance(2384, 500)
	engineSetModelLODDistance(2386, 500)
	engineSetModelLODDistance(2389, 500)
	engineSetModelLODDistance(2390, 500)
	engineSetModelLODDistance(2391, 500)
	engineSetModelLODDistance(2392, 500)
	engineSetModelLODDistance(2394, 500)
        engineSetModelLODDistance(2396, 500)

end 
)