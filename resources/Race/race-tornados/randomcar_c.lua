thePlayer = getLocalPlayer ()

----------------
--Vehicle Name--
----------------

local screenWidth, screenHeight = guiGetScreenSize()

function vehName ()
	theVehicle = getPedOccupiedVehicle (thePlayer)
	if theVehicle then
	vehType = getVehicleType (theVehicle)
	if vehType == "Bike" or vehType == "BMX" or vehType == "Quad" then											--if player's vehicle is a bike
		vehicleName = getVehicleName (theVehicle)
		dxDrawText ('Bike: ', 0, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
		dxDrawText (vehicleName, 60, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
	elseif vehType == "Automobile" then																		--if player's vehicle is a car
		vehicleName = getVehicleName (theVehicle)
		dxDrawText ('Car: ', 4, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
		dxDrawText (vehicleName, 60, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
	elseif vehType == "Plane" or vehType == "Helicopter" then													--if player's vehicle is a plane
		vehicleName = getVehicleName (theVehicle)
		dxDrawText ('Plane: ', 0, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
		dxDrawText (vehicleName, 75, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
	elseif vehType == "Boat" then																				--if player's vehicle is a boat
		vehicleName = getVehicleName (theVehicle)
		dxDrawText ('Boat: ', 0, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
		dxDrawText (vehicleName, 65, screenHeight-34, 0, 0, tocolor (255, 255, 255, 255), 0.7, 'bankgothic')
	end
	end
end
addEventHandler("onClientRender", getRootElement(), vehName)
