
	--carsids:
	--1st line: 2-Door and Compact cars (all)
	--2th line: 4-Door and Luxury cars (all)
	--3th line: Bikes (all)
	--4th line: Civil Servant / Public Transportation (no Baggage 485, Sweeper 574)
	--5th line: Government Vehicles (no Rhino 432)
	--6th line: Heavy and Utility Trucks (no Combine Harvester 532, Dozer 486, Dumper 406, Linerunner (From "Tanker Commando") 514, Tractor 531)
	--7th line: Light Trucks and Vans (no Forklift 530, Mower 572, Tug 583)
	--8th line: Lowriders (all)
	--9th line: Muscle Cars (all)
	--10th line: Recreational (no Caddy 457, Vortex 539)
	--11th line: Street Racers (all)
	--12th line: SUVs and Wagons (no Rancher (From "Lure") 505)
	local carids = 	{602, 496, 401, 518, 527, 589, 419, 533, 526, 474, 545, 517, 410, 600, 436, 580, 439, 549, 491,
			445, 604, 507, 585, 587, 466, 492, 546, 551, 516, 467, 426, 547, 405, 409, 550, 566, 540, 421, 529,
			581, 509, 481, 462, 521, 463, 510, 522, 461, 448, 468, 586,
			552, 431, 438, 437, 420, 525, 408,
			416, 433, 427, 490, 528, 407, 544, 523, 470, 598, 596, 597, 599, 601, 428,
			499, 609, 498, 524, 578, 573, 455, 588, 403, 423, 414, 443, 515, 456,
			459, 422, 482, 605, 418, 582, 413, 440, 543, 478, 554,
			536, 575, 534, 567, 535, 576, 412,
			402, 542, 603, 475,
			568, 424, 504, 483, 508, 571, 500, 444, 556, 557, 471, 495,
			429, 541, 415, 480, 562, 565, 434, 494, 502, 503, 411, 559, 561, 560, 506, 451, 558, 555, 477,
			579, 400, 404, 489, 479, 442, 458}

	--boatids: (all)
	local boatids = {472, 473, 493, 595, 484, 430, 453, 452, 446, 454}

	--planeids: (no Andromeda 592, AT-400 577, Cargobob 548, Hunter 425, Leviathan 417, Maverick 487, Nevada 553, News Chopper 488, Police Maverick 497, Raindance 563, Seasparrow 447, Sparrow 467)
	local planeids = {511, 512, 593, 476, 519, 460, 513}

    function randomCar(checkpoint, time)
        local veh = getPedOccupiedVehicle(source)

        if checkpoint == 1 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 2 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 3 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 4 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 5 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 6 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 7 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 8 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 9 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 10 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 11 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 12 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 13 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 14 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 15 then
			zmodifier(veh,1)
             setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 16 then
			zmodifier(veh,1)
             setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 17 then
			zmodifier(veh,1)
             setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 18 then
			zmodifier(veh,1)
             setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 19 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 20 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 21 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 22 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 23 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

        if checkpoint == 24 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

	if checkpoint == 25 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

	if checkpoint == 26 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

	if checkpoint == 27 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

	if checkpoint == 28 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

	if checkpoint == 29 then
			zmodifier(veh,1)
            setElementModel (veh, carids[math.random(#carids)])
	end

	if checkpoint == 30 then
			zmodifier(veh,1)
            setElementModel (veh, planeids[math.random(#planeids)])
	end

	if checkpoint == 31 then
			zmodifier(veh,1)
            setElementModel (veh, planeids[math.random(#planeids)])
	end

    end
    addEvent('onPlayerReachCheckpoint')
    addEventHandler('onPlayerReachCheckpoint', getRootElement(), randomCar)

	function zmodifier (theVehicle,height)
		x, y, z = getElementPosition (theVehicle)
		setElementPosition (theVehicle, x, y, z+height)
		setVehicleLocked (theVehicle, true)
		setVehicleDoorsUndamageable (theVehicle, true)
		setTimer (function ()
			setVehiclePanelState (theVehicle, 0, 0)
			setVehiclePanelState (theVehicle, 1, 0)
			setVehiclePanelState (theVehicle, 2, 0)
			setVehiclePanelState (theVehicle, 3, 0)
			setVehiclePanelState (theVehicle, 4, 0)
			setVehiclePanelState (theVehicle, 5, 0)
			setVehiclePanelState (theVehicle, 6, 0)
		end, 750, 1)
	end