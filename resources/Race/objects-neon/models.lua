
local models = {


[7578] = { txd = "textures.txd", dff = "blueneontube.dff",   col = "neon.col", lod=2000 },
[7577] = { txd = "textures.txd", dff = "greenneontube.dff",  col = "neon.col", lod=2000 },
[7576] = { txd = "textures.txd", dff = "pinkneontube.dff",   col = "neon.col", lod=2000 },
[7575] = { txd = "textures.txd", dff = "redneontube.dff",    col = "neon.col", lod=2000 },
[7574] = { txd = "textures.txd", dff = "whiteneontube.dff",  col = "neon.col", lod=2000 },
[7573] = { txd = "textures.txd", dff = "yellowneontube.dff", col = "neon.col", lod=2000 },

}


function ReplaceTexture(modelId, texture)
	if texture then
		local txd = engineLoadTXD(texture)
		if not txd then
			outputConsole(texture .." couldn't be loaded")
		else
			return engineImportTXD(txd, modelId)
		end
	end
	return false
end


function ReplaceModel(modelId, modelData)
	if modelData.dff then
		local dff = engineLoadDFF(modelData.dff, 0)
		if not dff then
			outputConsole(modelData.dff .." couldn't be loaded")
		else
			engineReplaceModel(dff, modelId)
		end
	end
	if modelData.col then
		local col = engineLoadCOL(modelData.col, modelId)
		if not col then
			outputConsole(modelData.col .." couldn't be loaded")
		else	
			engineReplaceCOL(col, modelId)
		end
	end	
	if modelData.lod then
		engineSetModelLODDistance(modelId, modelData.lod)
	end
end


addEventHandler("onClientResourceStart", getResourceRootElement(), 
	function()
		for modelId,modelData in pairs(models) do
			if ReplaceTexture(modelId, modelData.txd) then
				ReplaceModel(modelId, modelData)
			end
		end
	end 
)